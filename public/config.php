<?php

		
	if($_SERVER["SERVER_NAME"] == "localhost" || $_SERVER["SERVER_NAME"] == "127.0.0.1") {
		define("DATABASE_SERVER", "localhost");
		define("DATABASE_USERNAME", "root");
		define("DATABASE_PASSWORD", "");
		define("DATABASE_NAME", "exhibiciones");
	} else { 
		define("DATABASE_SERVER", "localhost");
		define("DATABASE_USERNAME", "root");
		define("DATABASE_PASSWORD", "");
		define("DATABASE_NAME", "exhibiciones	");
	}
	
	$dbcon = @mysqli_connect(DATABASE_SERVER, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME);
	if(!$dbcon) {
		echo '<!doctype html><html>
				<head>
					<meta charset="utf-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge">
					<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
					<link type="text/css" rel="stylesheet" href="'.$rootUrl.'manage/css/styles.min.css"/>
				</head>
				<body>
					<div class="header-main text-center" style="position:inherit"><img src="'.$rootUrl.'manage/images/logo.png" class="header-logo"></div>
					<div class="container">
						<div class="row">
							<div class="col-xs-12 text-center">
								<span style="font-weight:bold; font-size:30px; font-family:Arial, Helvetica, sans-serif;">Sitio en mantenimiento.</span>
							</div>
						</div>
					</div>
				</body>
			</html>';
		exit;
	}
	mysqli_query($dbcon, "SET NAMES UTF8");
	
	
?>