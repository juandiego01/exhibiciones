window.counterfind = 0;
window.originalContent = "";
window.originalContentCounter = 0;
function resaltarTexto(){
    if(window.originalContentCounter == 0){
      window.originalContent = $("#modalUsuariosContentTable").html();
      window.originalContentCounter ++;
    }

    var findvar = $("#searchUser").val();
    var textvar = window.originalContent;

    if(textvar.indexOf(findvar) == -1 ){
      $("#modalUsuariosContentTable").html(window.originalContent);
      $("#textResponse").html("No se encontraron resultados");
      return; 
    }

    if(findvar.indexOf("table") >= 0 || findvar.indexOf("<") >= 0 || findvar.indexOf(">") >= 0 || findvar.indexOf("<td") >= 0 || findvar.indexOf("<tr") >= 0 || findvar.indexOf('"') >= 0){
      $("#modalUsuariosContentTable").html(window.originalContent);
      $("#textResponse").html("No se encontraron resultados");
      return; 
    }
    if($("#searchUser").val() == ""){
      $("#modalUsuariosContentTable").html(window.originalContent);
      $("#textResponse").html("Escriba un texto para buscar");
      return; 
    }
    window.counterfind++;
    var str = window.originalContent;
    if($("#searchUser").val().length > 3){
      var txttemp = $("#searchUser").val();
      var expreg = new RegExp($("#searchUser").val(), "g");

      var res = str.replace(expreg, "<span id='anchorvar_" + window.counterfind + "' style='position:relative;' class='resaltarTexto'>" + txttemp + "</span>");

      $("#modalUsuariosContentTable").html(res);

      $("#textResponse").html("<i class='fa fa-check-circle' style='font-size:20px;' aria-hidden='true'></i>");
      
      setChecksUsuariosAlmacen(0);     

      scrollToAnchor("anchorvar_" + window.counterfind);    
    }
}
function scrollToAnchor(aid){
    //alert($("#anchorvar").offset().top);
    var aTag = $("#" + aid);
    $('#usuarioAlmacenModal').animate({scrollTop: aTag.offset().top - 10},'slow');
}


window.counterfindSur = 0;
window.originalContentSur = "";
window.originalContentCounterSur = 0;

function resaltarTextoSur(){
    if(window.originalContentCounterSur == 0){
      window.originalContentSur = $("#modalUsuariosContentTableSur").html();
      window.originalContentCounterSur ++;
    }

    var findvar = $("#searchUserSur").val();
    var textvar = window.originalContentSur;

    if(textvar.indexOf(findvar) == -1 ){
      $("#modalUsuariosContentTableSur").html(window.originalContentSur);
      $("#textResponseSur").html("No se encontraron resultados");
      return; 
    }

    if(findvar.indexOf("table") >= 0 || findvar.indexOf("<") >= 0 || findvar.indexOf(">") >= 0 || findvar.indexOf("<td") >= 0 || findvar.indexOf("<tr") >= 0 || findvar.indexOf('"') >= 0){
      $("#modalUsuariosContentTableSur").html(window.originalContentSur);
      $("#textResponseSur").html("No se encontraron resultados");
      return; 
    }
    if($("#searchUserSur").val() == ""){
      $("#modalUsuariosContentTableSur").html(window.originalContentSur);
      $("#textResponseSur").html("Escriba un texto para buscar");
      return; 
    }
    window.counterfindSur++;
    var str = window.originalContentSur;
    if($("#searchUserSur").val().length > 3){
      var txttemp = $("#searchUserSur").val();
      var expreg = new RegExp($("#searchUserSur").val(), "g");

      var res = str.replace(expreg, "<span id='anchorvarsur_" + window.counterfindSur + "' style='position:relative;' class='resaltarTexto'>" + txttemp + "</span>");

      $("#modalUsuariosContentTableSur").html(res);

      $("#textResponseSur").html("<i class='fa fa-check-circle' style='font-size:20px;' aria-hidden='true'></i>");
      
      setChecksUsuariosAlmacen(1);     

      scrollToAnchorSur("anchorvarsur_" + window.counterfindSur);    
    }
}
function scrollToAnchorSur(aid){
    //alert($("#anchorvar").offset().top);
    var aTag = $("#" + aid);
    $('#usuarioSurtidoModal').animate({scrollTop: aTag.offset().top - 10},'slow');
}
var usuariosAlmacen = new Array();
var usuariosSurtido = new Array();


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {
  window.addVar = 0;
}else{
  window.addVar = 17;
}