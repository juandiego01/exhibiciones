<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180410195217 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuarios DROP usuario, DROP contrasena, DROP fecha, DROP acceso_permitido, DROP eliminado');
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuarios ADD usuario VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci, ADD contrasena VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci, ADD fecha DATETIME NOT NULL, ADD acceso_permitido TINYINT(1) NOT NULL, ADD eliminado TINYINT(1) NOT NULL');
    }
}
