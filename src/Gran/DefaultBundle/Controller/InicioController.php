<?php
// src/Gran/DefaultBundle/Controller/InicioController.php
namespace App\Gran\DefaultBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Firebase\JWT\JWT;
use App\Gran\DefaultBundle\Entity\Usuarios;
use App\Gran\DefaultBundle\Entity\Proveedores;

class InicioController extends Controller
{    
    public function index(Request $request)
    {
        return $this->redirectToRoute('usuario_inicio');             
    }

    public function loginsip(Request $request)
    {
        $datos = $request->request->all();
        $sendlogin = "false";
        $retvar = "";
        $entityManager = $this->getDoctrine()->getManager();

        if(@$datos["loginvar"]){
            $retvar = "Iniciando sesión"; 

            $url = "https://sipasociados.coopidrogas.com.co/api_third_parties/login_check";
            $data = array('username' => $datos["_username"], 'password' => $datos["_password"]); //'Cota2019'
            $options = array('http' => array(
                    'header'  => ["Content-Type: application/json"],
                    'method'  => 'POST','content' => json_encode($data)
                ),'ssl' => ['verify_peer' => true]
            );

$publicKey = <<<EOD

-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAlCxLhr3HT/D5sP3uIXxz
WDp29LqH2e6zoDUWc5R30Dv5vvlpCywq70AYBhQFHZbVmT85OcEvWT23wDEr8hQl
9bQTz0r5iH6bqjGk0dL+1yHo2KDcbhxqt8rnxdPq0QR5mOwTEo+qJiQfzkchxbVB
oTXcWglym32SO35CxkiTlORa1ex4JyH9Xv9PIfb/f+42DWAUfEGhMN/04rVxLGGi
dlkreJcMspVWbV6GvjPL7r6TE1ZUA2sTjpgtW5yQoTZoyy4be2stFPUyuNPwT50K
ExXS17gURpQQkUvgXKiafnG0Zpsj0oZTdiJfdReFWlqjwHYhOkFdyONX2Fg5tSpV
OYFK7Dfn/euqEFta3S7PWdZsuv0/5xDAqbyL5/0366hINtz3BaERRl7cHpmv6/nt
yhN77VCE/+LWr3Ls6MdRHf3t4rseCwzhY/y54aFpA0MwNBavbQvM0wAAnKsIQWdw
bBxrAOPni+GgxcDMyncJlIm5A4PDU2L9e5u7Ru0cBluH5FyPl4dPO8Zv8swGrcLN
OifNC3SjwAh6awIUyo+WwphhgxzSBbea4qbk/FbGYrbupJpMcS5Kra9r3JwpxrVI
oBqAZfimEx8u6O9v7qIICbfF21fqOrrGbGIuhTS31T2gUCkk/MeczAqcYkVSvskN
jaPDal9HCH3Lx909mzZG5CMCAwEAAQ==
-----END PUBLIC KEY-----

EOD;
                $context  = stream_context_create($options);           
                if(!$result = @file_get_contents($url, false, $context)){
                    $retvar = "error"; 
                }else{                    
                    $sendlogin = "true";
                    $resultado=json_decode($result);
                    
                    $decoded = JWT::decode($resultado->token, $publicKey, array('RS256'));
                    $decoded_array = (array) $decoded;

                     $verificarUsuarioLocal = $this->getDoctrine()->getRepository("GranDefaultBundle:Usuarios")->createQueryBuilder('u')
                         ->where('u.usuario = :usuario')->setParameter('usuario', $datos["_username"])
                         ->getQuery()->execute();
                    $idUsuario = 0;
                    foreach ($verificarUsuarioLocal as $key => $value) {
                        $idUsuario = $value->getId();
                    }

                    $objInfo = $decoded_array["info_basica"];
                    $centro = $objInfo->centroCosto;
                    $centro = substr($centro, 0,1);
                    $centro = $centro."100";

                    if($idUsuario == 0){
                        $usuario = new Usuarios();                        
                        
                        $usuario->setUsuario($datos["_username"]);
                        $usuario->setContrasena($datos["_password"]);
                        $usuario->setFecha(new \DateTime(date("Y-m-d H:i:s")));                        

                        $usuario->setCiudad($objInfo->ciudad);
                        $usuario->setRegional($centro);
                        $usuario->setCodigo($datos["_username"]);
                        
                        $usuario->setDrogueria($objInfo->drogueria);
                        $usuario->setDireccion($objInfo->direcion);
                        $usuario->setTelefono($objInfo->telefono);

                        $usuario->setNombreGerente(0);
                        $usuario->setMostrador(0);
                        $usuario->setAutoservicio(0);
                        $usuario->setEntradasSalidas(0);
                        $usuario->setPuntosPago(0);
                        $usuario->setNumeroVendedores(0);
                        $usuario->setArea(0);
                        $usuario->setAccesoPermitido(1);                       
                        $usuario->setEliminado(0);
                        
                        $entityManager->persist($usuario);
                        $entityManager->flush($usuario);

                    }else{
                        $usuario = $entityManager->getRepository('GranDefaultBundle:Usuarios')->find($idUsuario);

                        $usuario->setContrasena($datos["_password"]);

                        $usuario->setCiudad($objInfo->ciudad);
                        $usuario->setRegional($centro);
                        $usuario->setCodigo($datos["_username"]);
                        
                        $usuario->setDrogueria($objInfo->drogueria);
                        $usuario->setDireccion($objInfo->direcion);
                        $usuario->setTelefono($objInfo->telefono);

                        $entityManager->persist($usuario);
                        $entityManager->flush($usuario);
                    }
                }
            }
        
        return $this->render('Default/inicio/indexsip.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'retvar' => $retvar,
            'sendlogin' => $sendlogin,
            'username' => @$datos["_username"], 
            'password' => @$datos["_password"],
                ));   
    }

    //------------------------------------------------------------------------------

    public function loginproveedorsip(Request $request)
    {
        $datos = $request->request->all();
        $sendlogin = "false";
        $retvar = "";
        $entityManager = $this->getDoctrine()->getManager();

        if(@$datos["loginvar"]){
            $retvar = "Iniciando sesión"; 

            $url = "https://sipproveedores.coopidrogas.com.co/api_proveedor/login_check";
            $data = array('_username' => $datos["_username"], '_password' => $datos["_password"]);
           // $data = array('_username' => "Abbott2016", '_password' => "Abbott2020");

            $options = array('http' => array(
                    'header'  => ["Content-Type: application/json"],
                    'method'  => 'POST','content' => json_encode($data)
                ),'ssl' => ['verify_peer' => true]
            );

$publicKey = <<<EOD

-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxuajREmXnW+/VOTHOi0n
d+d9NhacXs9DmcYpHfAzyd+A8HBxKuH3+1J2l+4ZpMfJW27isGyg0ZpUbb8zLfqg
7daP7SXAxPkI1XI4I+P5AOY0tWXBuIDOlLK8Zm8BCntjgLIi0det5w7oJNsiditQ
Ts+EX5HqRMNEngFymx/+8sVhUILvl1g3TLFXfdMNa0GXtZsnV9vl9Kd+EQ8cpA0z
fKW5qI/ommHobytIzc8afrfBmj7EgIVe8KYtNLxU8na32W6dhWEjDhIdSWUfUKrG
TJOlNkR2OPA8DqzuuHCyDKhIBkK0b0OCAgmSOgnUubqpY206TKB8jFQfAgX+Ynij
c6gC2hDyzTZyWEIExkD0OQHNCdLubgBA2mLHfqM/xs+R7jZ0+JplAkp+4le5jA9z
9lRe7HQ/6axOu2AH1jVAkpaKqIU7mNWZt1JuLDn1XMzQiVeBZywXS6i1MAv4yDs4
0Gy8K3RzSPzBKQ2T2eZVOhB70ZZ0ZlcSeUp0pVk9DWYLmZLpuCOo9u+GhzR1Bwig
m8qcDDybTZFbmgjWVUfzxZOYAHsaqFSmlQzKVc4R/IgxbOeEwdshaKfavyLVpe63
BU1qJy/WJ8qwmFXdpmUmWGavxrHSrojuzDtQWaIrvTcWTGOr/iLktETypwVUyJni
qfxaXT41zSeSUPWALblRcdkCAwEAAQ==
-----END PUBLIC KEY-----

EOD;
                $context  = stream_context_create($options);           
                $result = @file_get_contents($url, false, $context);
                $resultado=json_decode($result);
                

                $results = 0;
                if(!$resultado){
                    $retvar = "error"; 
                }else{ 

                    $sendlogin = "true";                    
                    
                    $decoded = JWT::decode($resultado->token, $publicKey, array('RS256'));
                    $decoded_array = (array) $decoded;

                    $verificarUsuarioLocal = $this->getDoctrine()->getRepository("GranDefaultBundle:Proveedores")->createQueryBuilder('u')
                         ->where('u.usuario = :usuario')->setParameter('usuario', $datos["_username"])
                         ->getQuery()->execute();
                    $idUsuario = 0;
                    foreach ($verificarUsuarioLocal as $key => $value) {
                        $idUsuario = $value->getId();
                    }

                    if($idUsuario == 0){
                        $proveedor = new Proveedores();                        
                        
                        $proveedor->setUsuario($datos["_username"]);
                        $proveedor->setContrasena($datos["_password"]);
                        $proveedor->setFecha(new \DateTime(date("Y-m-d H:i:s")));

                        //$proveedor->setNombre($objInfo->razonSocial);                        
                        //$proveedor->setCodigo($objInfo->codigoCopidrogas);                                               

                        $proveedor->setNombre(1);                        
                        $proveedor->setCodigo(1);  
                        $proveedor->setAccesoPermitido(1);    
                        $proveedor->setEliminado(0);                                         
                        
                        $entityManager->persist($proveedor);
                        $entityManager->flush($proveedor);

                    }else{
                        $proveedor = $entityManager->getRepository('GranDefaultBundle:Proveedores')->find($idUsuario);

                        $proveedor->setUsuario($datos["_username"]);
                        $proveedor->setContrasena($datos["_password"]);

                        $proveedor->setNombre(1);
                        $proveedor->setCodigo(1);                        

                        $entityManager->persist($proveedor);
                        $entityManager->flush($proveedor);
                    }
                }
            }
        
        return $this->render('Default/inicio/indexproveedorsip.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'retvar' => $retvar,
            'sendlogin' => $sendlogin,
            'username' => @$datos["_username"], 
            'password' => @$datos["_password"],
                ));   
    }
}