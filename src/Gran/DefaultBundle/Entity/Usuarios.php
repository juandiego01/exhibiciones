<?php

namespace App\Gran\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios", uniqueConstraints={@ORM\UniqueConstraint(name="codigo", columns={"codigo"})})
 * @ORM\Entity
 */
class Usuarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="contrasena", type="string", length=255, nullable=false)
     */
    private $contrasena;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=255, nullable=false)
     */
    private $ciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="regional", type="string", length=255, nullable=false)
     */
    private $regional;

    /**
     * @var int
     *
     * @ORM\Column(name="codigo", type="integer", nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="drogueria", type="string", length=255, nullable=false)
     */
    private $drogueria;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=false)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_gerente", type="string", length=255, nullable=false)
     */
    private $nombreGerente;

    /**
     * @var string
     *
     * @ORM\Column(name="mostrador", type="string", length=255, nullable=false)
     */
    private $mostrador;

    /**
     * @var string
     *
     * @ORM\Column(name="autoservicio", type="string", length=255, nullable=false)
     */
    private $autoservicio;

    /**
     * @var string
     *
     * @ORM\Column(name="entradas_salidas", type="string", length=255, nullable=false)
     */
    private $entradasSalidas;

    /**
     * @var string
     *
     * @ORM\Column(name="puntos_pago", type="string", length=255, nullable=false)
     */
    private $puntosPago;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_vendedores", type="string", length=255, nullable=false)
     */
    private $numeroVendedores;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=255, nullable=false)
     */
    private $area;

    /**
     * @var bool
     *
     * @ORM\Column(name="acceso_permitido", type="boolean", nullable=false)
     */
    private $accesoPermitido;

    /**
     * @var bool
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=false)
     */
    private $eliminado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuario(): ?string
    {
        return $this->usuario;
    }

    public function setUsuario(string $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getContrasena(): ?string
    {
        return $this->contrasena;
    }

    public function setContrasena(string $contrasena): self
    {
        $this->contrasena = $contrasena;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getCiudad(): ?string
    {
        return $this->ciudad;
    }

    public function setCiudad(string $ciudad): self
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getRegional(): ?string
    {
        return $this->regional;
    }

    public function setRegional(string $regional): self
    {
        $this->regional = $regional;

        return $this;
    }

    public function getCodigo(): ?int
    {
        return $this->codigo;
    }

    public function setCodigo(int $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getDrogueria(): ?string
    {
        return $this->drogueria;
    }

    public function setDrogueria(string $drogueria): self
    {
        $this->drogueria = $drogueria;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getNombreGerente(): ?string
    {
        return $this->nombreGerente;
    }

    public function setNombreGerente(string $nombreGerente): self
    {
        $this->nombreGerente = $nombreGerente;

        return $this;
    }

    public function getMostrador(): ?string
    {
        return $this->mostrador;
    }

    public function setMostrador(string $mostrador): self
    {
        $this->mostrador = $mostrador;

        return $this;
    }

    public function getAutoservicio(): ?string
    {
        return $this->autoservicio;
    }

    public function setAutoservicio(string $autoservicio): self
    {
        $this->autoservicio = $autoservicio;

        return $this;
    }

    public function getEntradasSalidas(): ?string
    {
        return $this->entradasSalidas;
    }

    public function setEntradasSalidas(string $entradasSalidas): self
    {
        $this->entradasSalidas = $entradasSalidas;

        return $this;
    }

    public function getPuntosPago(): ?string
    {
        return $this->puntosPago;
    }

    public function setPuntosPago(string $puntosPago): self
    {
        $this->puntosPago = $puntosPago;

        return $this;
    }

    public function getNumeroVendedores(): ?string
    {
        return $this->numeroVendedores;
    }

    public function setNumeroVendedores(string $numeroVendedores): self
    {
        $this->numeroVendedores = $numeroVendedores;

        return $this;
    }

    public function getArea(): ?string
    {
        return $this->area;
    }

    public function setArea(string $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getAccesoPermitido(): ?bool
    {
        return $this->accesoPermitido;
    }

    public function setAccesoPermitido(bool $accesoPermitido): self
    {
        $this->accesoPermitido = $accesoPermitido;

        return $this;
    }

    public function getEliminado(): ?bool
    {
        return $this->eliminado;
    }

    public function setEliminado(bool $eliminado): self
    {
        $this->eliminado = $eliminado;

        return $this;
    }


}
