<?php
// src/Gran/ProveedorBundle/Controller/EspaciosVendidosController.php
namespace App\Gran\ProveedorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\ProveedorBundle\Entity\EspaciosVendidos;
use App\Gran\ProveedorBundle\Form\EspaciosVendidosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Cpdg\UsuarioBundle\Controller\GlobalesController;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class EspaciosVendidosController extends Controller
{    
    public function inicioAction(Request $request)
    {        
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idProveedor=$objUsuario->getId();

        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciosVendidos = $this->getDoctrine()->getRepository("GranProveedorBundle:EspaciosVendidos")->createQueryBuilder('ev')
        ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
        ->andWhere('ev.idProveedor = :idProveedor')->setParameter('idProveedor', $idProveedor)
        ->leftJoin('GranProveedorBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.id = ev.idEspacio')
        ->leftJoin('GranUsuarioBundle:EspaciosVendidosEstados', 'eve', 'WITH', 'eve.id = ev.idEstado')
        ->leftJoin('GranProveedorBundle:Proveedores', 'p', 'WITH', 'p.id = ev.idProveedor')
        ->leftJoin('GranProveedorBundle:Usuarios', 'u', 'WITH', 'u.id = ev.idUsuario');
        
        if(@$request->query->get('buscar') != ""){
           $espaciosVendidos->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('p.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('u.usuario LIKE :usuario1')->setParameter('usuario1', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('ev.proveedorNombre LIKE :proveedorNombre')->setParameter('proveedorNombre', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('eve.estado LIKE :estado')->setParameter('estado', "%".$request->query->get('buscar')."%");
        }

        $espaciosVendidosExecute = $espaciosVendidos->orderBy("ev.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciosVendidosExecute,
            $pagev,$cantv
        );

        return $this->render('Proveedor/espaciosvendidos/index.html.twig', array(
            'titulo' => "Espacios negociados",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'buscar' => @$request->query->get('buscar'),            
                ));        
    }
}