<?php
// src/Gran/ProveedorBundle/Controller/EspaciospublicitariosController.php
namespace App\Gran\ProveedorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\ProveedorBundle\Entity\EspaciosPublicitarios;
use App\Gran\ProveedorBundle\Form\EspaciosPublicitariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Cpdg\UsuarioBundle\Controller\GlobalesController;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class EspaciospublicitariosController extends Controller
{    
    public function inicioAction(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciospublicitarios = $this->getDoctrine()->getRepository("GranProveedorBundle:EspaciosPublicitarios")->createQueryBuilder('ep')        
        ->leftJoin('GranProveedorBundle:Administradores', 'a', 'WITH', 'a.id = ep.idAdministrador')
        ->leftJoin('GranProveedorBundle:EspaciosPublicitariosTipos', 'ept', 'WITH', 'ept.id = ep.idTipo');
        
        if(@$request->query->get('buscar') != ""){
           $espaciospublicitarios->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");           
           $espaciospublicitarios->orWhere('ep.descripcion LIKE :descripcion')->setParameter('descripcion', "%".$request->query->get('buscar')."%");
           $espaciospublicitarios->orWhere('a.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");           
        }

        $espaciospublicitarios->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
                              ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 1);

        $espaciospublicitariosExecute = $espaciospublicitarios->orderBy("ep.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciospublicitariosExecute,
            $pagev,$cantv
        );

        return $this->render('Proveedor/espaciospublicitarios/index.html.twig', array(
            'titulo' => "Espacios pre-definidos",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'buscar' => @$request->query->get('buscar'),            
                ));        
    }
}