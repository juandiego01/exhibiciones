<?php

namespace App\Gran\ProveedorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EspaciosVendidos
 *
 * @ORM\Table(name="espacios_vendidos", indexes={@ORM\Index(name="id_proveedor", columns={"id_proveedor"}), @ORM\Index(name="id_proveedor_solicitud", columns={"id_proveedor_solicitud"}), @ORM\Index(name="id_usuariokoala_solicitud", columns={"id_usuariokoala_solicitud"}), @ORM\Index(name="id_administrador_solicitud", columns={"id_administrador_solicitud"}), @ORM\Index(name="id_usuario", columns={"id_usuario"}), @ORM\Index(name="id_estado", columns={"id_estado"}), @ORM\Index(name="espacios_vendidos_ibfk_5", columns={"id_espacio"})})
 * @ORM\Entity
 */
class EspaciosVendidos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicial", type="date", nullable=false)
     */
    private $fechaInicial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_final", type="date", nullable=false)
     */
    private $fechaFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="string", length=255, nullable=false)
     */
    private $valor;

    /**
     * @var string
     *
     * @ORM\Column(name="foto_espacio", type="string", length=255, nullable=false)
     */
    private $fotoEspacio;

    /**
     * @var string
     *
     * @ORM\Column(name="foto_material", type="string", length=255, nullable=false)
     */
    private $fotoMaterial;

    /**
     * @var string
     *
     * @ORM\Column(name="proveedor_nombre", type="string", length=255, nullable=false)
     */
    private $proveedorNombre;

    /**
     * @var int
     *
     * @ORM\Column(name="proveedor_cedula", type="integer", nullable=false)
     */
    private $proveedorCedula;

    /**
     * @var string
     *
     * @ORM\Column(name="proveedor_celular", type="string", length=255, nullable=false)
     */
    private $proveedorCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="proveedor_cargo", type="string", length=255, nullable=false)
     */
    private $proveedorCargo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_radicacion", type="datetime", nullable=false)
     */
    private $fechaRadicacion;

    /**
     * @var bool
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=false)
     */
    private $eliminado;

    /**
     * @var \Proveedores
     *
     * @ORM\ManyToOne(targetEntity="Proveedores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_proveedor", referencedColumnName="id")
     * })
     */
    private $idProveedor;

    /**
     * @var \Proveedores
     *
     * @ORM\ManyToOne(targetEntity="Proveedores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_proveedor_solicitud", referencedColumnName="id")
     * })
     */
    private $idProveedorSolicitud;

    /**
     * @var \Usuarioskoala
     *
     * @ORM\ManyToOne(targetEntity="Usuarioskoala")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuariokoala_solicitud", referencedColumnName="id")
     * })
     */
    private $idUsuariokoalaSolicitud;

    /**
     * @var \Administradores
     *
     * @ORM\ManyToOne(targetEntity="Administradores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_administrador_solicitud", referencedColumnName="id")
     * })
     */
    private $idAdministradorSolicitud;

    /**
     * @var \EspaciosPublicitarios
     *
     * @ORM\ManyToOne(targetEntity="EspaciosPublicitarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_espacio", referencedColumnName="id")
     * })
     */
    private $idEspacio;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @var \EspaciosVendidosEstados
     *
     * @ORM\ManyToOne(targetEntity="EspaciosVendidosEstados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado", referencedColumnName="id")
     * })
     */
    private $idEstado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaInicial(): ?\DateTimeInterface
    {
        return $this->fechaInicial;
    }

    public function setFechaInicial(\DateTimeInterface $fechaInicial): self
    {
        $this->fechaInicial = $fechaInicial;

        return $this;
    }

    public function getFechaFinal(): ?\DateTimeInterface
    {
        return $this->fechaFinal;
    }

    public function setFechaFinal(\DateTimeInterface $fechaFinal): self
    {
        $this->fechaFinal = $fechaFinal;

        return $this;
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValor(string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getFotoEspacio(): ?string
    {
        return $this->fotoEspacio;
    }

    public function setFotoEspacio(string $fotoEspacio): self
    {
        $this->fotoEspacio = $fotoEspacio;

        return $this;
    }

    public function getFotoMaterial(): ?string
    {
        return $this->fotoMaterial;
    }

    public function setFotoMaterial(string $fotoMaterial): self
    {
        $this->fotoMaterial = $fotoMaterial;

        return $this;
    }

    public function getProveedorNombre(): ?string
    {
        return $this->proveedorNombre;
    }

    public function setProveedorNombre(string $proveedorNombre): self
    {
        $this->proveedorNombre = $proveedorNombre;

        return $this;
    }

    public function getProveedorCedula(): ?int
    {
        return $this->proveedorCedula;
    }

    public function setProveedorCedula(int $proveedorCedula): self
    {
        $this->proveedorCedula = $proveedorCedula;

        return $this;
    }

    public function getProveedorCelular(): ?string
    {
        return $this->proveedorCelular;
    }

    public function setProveedorCelular(string $proveedorCelular): self
    {
        $this->proveedorCelular = $proveedorCelular;

        return $this;
    }

    public function getProveedorCargo(): ?string
    {
        return $this->proveedorCargo;
    }

    public function setProveedorCargo(string $proveedorCargo): self
    {
        $this->proveedorCargo = $proveedorCargo;

        return $this;
    }

    public function getFechaRadicacion(): ?\DateTimeInterface
    {
        return $this->fechaRadicacion;
    }

    public function setFechaRadicacion(\DateTimeInterface $fechaRadicacion): self
    {
        $this->fechaRadicacion = $fechaRadicacion;

        return $this;
    }

    public function getEliminado(): ?bool
    {
        return $this->eliminado;
    }

    public function setEliminado(bool $eliminado): self
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    public function getIdProveedor(): ?Proveedores
    {
        return $this->idProveedor;
    }

    public function setIdProveedor(?Proveedores $idProveedor): self
    {
        $this->idProveedor = $idProveedor;

        return $this;
    }

    public function getIdProveedorSolicitud(): ?Proveedores
    {
        return $this->idProveedorSolicitud;
    }

    public function setIdProveedorSolicitud(?Proveedores $idProveedorSolicitud): self
    {
        $this->idProveedorSolicitud = $idProveedorSolicitud;

        return $this;
    }

    public function getIdUsuariokoalaSolicitud(): ?Usuarioskoala
    {
        return $this->idUsuariokoalaSolicitud;
    }

    public function setIdUsuariokoalaSolicitud(?Usuarioskoala $idUsuariokoalaSolicitud): self
    {
        $this->idUsuariokoalaSolicitud = $idUsuariokoalaSolicitud;

        return $this;
    }

    public function getIdAdministradorSolicitud(): ?Administradores
    {
        return $this->idAdministradorSolicitud;
    }

    public function setIdAdministradorSolicitud(?Administradores $idAdministradorSolicitud): self
    {
        $this->idAdministradorSolicitud = $idAdministradorSolicitud;

        return $this;
    }

    public function getIdEspacio(): ?EspaciosPublicitarios
    {
        return $this->idEspacio;
    }

    public function setIdEspacio(?EspaciosPublicitarios $idEspacio): self
    {
        $this->idEspacio = $idEspacio;

        return $this;
    }

    public function getIdUsuario(): ?Usuarios
    {
        return $this->idUsuario;
    }

    public function setIdUsuario(?Usuarios $idUsuario): self
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    public function getIdEstado(): ?EspaciosVendidosEstados
    {
        return $this->idEstado;
    }

    public function setIdEstado(?EspaciosVendidosEstados $idEstado): self
    {
        $this->idEstado = $idEstado;

        return $this;
    }


}
