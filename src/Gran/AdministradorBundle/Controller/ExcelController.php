<?php
// src/Gran/AdministradorBundle/Controller/ExcelController.php
namespace App\Gran\AdministradorBundle\Controller;

use App\Gran\AdministradorBundle\Entity\Reportes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
 
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Gran\AdministradorBundle\Conhtroller\GlobalesController;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

use PhpOffice\PhpSpreadsheet\Factory;

class ExcelController extends Controller
{
    
    public function excel1Action(Request $request)
    {
        $useridObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $userid=$useridObj->getId();
        
        $em = $this->getDoctrine()->getManager(); 
        $data = $request->request->all();



        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();

        $xrow = 1;
        $activeSheet->setCellValue("A".$xrow , 'Estado')->getStyle("A".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("B".$xrow , 'Solicitado por')->getStyle("B".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("C".$xrow , 'Nombre Espacio')->getStyle("C".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("D".$xrow , 'Descripción')->getStyle("D".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("E".$xrow , 'Alto')->getStyle("E".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("F".$xrow , 'Ancho')->getStyle("F".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("G".$xrow , 'Fecha de Solicitud')->getStyle("G".$xrow)->getFont()->setBold(true);

        $activeSheet->getColumnDimension('A')->setAutoSize(true);
        $activeSheet->getColumnDimension('B')->setAutoSize(true);
        $activeSheet->getColumnDimension('C')->setAutoSize(true);
        $activeSheet->getColumnDimension('D')->setAutoSize(true);
        $activeSheet->getColumnDimension('E')->setAutoSize(true);
        $activeSheet->getColumnDimension('F')->setAutoSize(true);
        $activeSheet->getColumnDimension('G')->setAutoSize(true);

        $espaciospublicitarios = $this->getDoctrine()->getRepository("GranAdministradorBundle:EspaciosPublicitarios")->createQueryBuilder('ep')        

        ->leftJoin('GranAdministradorBundle:Usuarios', 'u', 'WITH', 'u.id = ep.idUsuario')
        ->leftJoin('GranAdministradorBundle:EspaciosPublicitariosTipos', 'ept', 'WITH', 'ept.id = ep.idTipo')
        ->leftJoin('GranAdministradorBundle:EspaciosPublicitariosEstados', 'epe', 'WITH', 'epe.id = ep.idEstado')
        ;
        
        if(@$request->query->get('b') != ""){
           $espaciospublicitarios->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('b')."%");           
           $espaciospublicitarios->orWhere('ep.descripcion LIKE :descripcion')->setParameter('descripcion', "%".$request->query->get('b')."%");
           $espaciospublicitarios->orWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('b')."%");  
           $espaciospublicitarios->orWhere('epe.estado LIKE :estado')->setParameter('estado', "%".$request->query->get('b')."%");          
        }
        $espaciospublicitarios->andWhere('ep.idTipoEspacio != :idTipoEspacio')->setParameter('idTipoEspacio', "");           

        $espaciospublicitarios->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
                              ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 2);

        $espaciospublicitariosExecute = $espaciospublicitarios->orderBy("ep.id","desc")->getQuery()->execute();

        foreach ($espaciospublicitariosExecute as $key => $value) {
            $xrow++;
            $activeSheet->setCellValue("A".$xrow , $value->getIdEstado()->getEstado())->getStyle("A".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("B".$xrow , $value->getIdUsuario()->getUsuario())->getStyle("B".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("C".$xrow , $value->getIdTipoEspacio()->getNombre())->getStyle("C".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("D".$xrow , $value->getDescripcion())->getStyle("D".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("E".$xrow , $value->getAlto())->getStyle("E".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("F".$xrow , $value->getAncho())->getStyle("F".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("G".$xrow , $value->getFechaCreacion()->format("Y-m-d H:i:s"))->getStyle("G".$xrow)->getFont()->setBold(false);
        }        

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=EspaciosPublicitarios.xls');
        header('Cache-Control: max-age=0');         
        $Excel_writer->save('php://output');
    }
    
    public function excel2Action(Request $request)
    {
        
        $useridObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $userid=$useridObj->getId();        
        
        $em = $this->getDoctrine()->getManager(); 
        $data = $request->request->all();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();

        $xrow = 1;
        $activeSheet->setCellValue("A".$xrow , 'Código')->getStyle("A".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("B".$xrow , 'Asociado')->getStyle("B".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("C".$xrow , 'Total Espacios')->getStyle("C".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("D".$xrow , 'Espacios negociados vigentes')->getStyle("D".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("E".$xrow , 'Espacios disponibles')->getStyle("E".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("F".$xrow , 'Total historico instalados')->getStyle("F".$xrow)->getFont()->setBold(true);        

        $activeSheet->getColumnDimension('A')->setAutoSize(true);
        $activeSheet->getColumnDimension('B')->setAutoSize(true);
        $activeSheet->getColumnDimension('C')->setAutoSize(true);
        $activeSheet->getColumnDimension('D')->setAutoSize(true);
        $activeSheet->getColumnDimension('E')->setAutoSize(true);
        $activeSheet->getColumnDimension('F')->setAutoSize(true);        
        
        $idsArrayBase = array();
        $espaciospublicitariosdefinidos = $this->getDoctrine()->getRepository("GranAdministradorBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 1);
        $arrayDefinidos = array();
        $predefinidos = "";
        $predefinidosCount = 0;
        foreach ($espaciospublicitariosdefinidos->getQuery()->execute() as $k => $v) {
            $arrayDefinidos[$v->getId()]["id"] = $v->getId();
            $arrayDefinidos[$v->getId()]["nombre"] = $v->getNombreEspacio();
            $predefinidos .= $v->getNombreEspacio().", ";
            $predefinidosCount++;
            $idsArrayBase[] = $v->getId();
        }
        $predefinidos = substr($predefinidos, 0, -2);        
        
        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $usuariosList = $this->getDoctrine()->getRepository("GranAdministradorBundle:Usuarios")->createQueryBuilder('u')
        ->andWhere('u.eliminado = :eliminado')->setParameter('eliminado', 0);

        $usuarios = $this->getDoctrine()->getRepository("GranAdministradorBundle:Usuarios")->createQueryBuilder('u');
        
        if(@$request->query->get('b') != ""){
           $usuarios->andWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('b')."%")
           ->orWhere('u.drogueria LIKE :drogueria')->setParameter('drogueria', "%".$request->query->get('b')."%");  

           $usuariosList->andWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('b')."%")
           ->orWhere('u.drogueria LIKE :drogueria')->setParameter('drogueria', "%".$request->query->get('b')."%");        
        }

        $usuarios->andWhere('u.eliminado = :eliminado')->setParameter('eliminado', 0);

        $solicitadosArray = array(); 
        $vendidosArray = array(); 
        $disponiblesArray = array(); 
        $totalesArray = array(); 


        $usuariosListExecute = $usuariosList->orderBy("u.codigo","asc")->getQuery()->execute();
        
        foreach ($usuariosListExecute as $k1 => $v1) {
            $solicitados = "";
            $solicitadosCount = 0;
            $idsArray = array();
            $espaciospublicitariossoliciados = $this->getDoctrine()->getRepository("GranAdministradorBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ep.idEstado = :idEstado')->setParameter('idEstado', 2)
            ->andWhere('ep.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId());
            
            foreach ($espaciospublicitariossoliciados->getQuery()->execute() as $k2 => $v2){
                $solicitados .= $v2->getNombreEspacio().", ";
                $solicitadosCount++;
                $idsArray[$v2->getId()] = $v2->getId();
            }
            $solicitados = substr($solicitados, 0, -2);            
            $solicitadosArray[$v1->getId()] = $solicitadosCount;

            $arrayIn = array_merge($idsArrayBase, $idsArray);
            $now = date("Y-m-d");
            $espaciospublicitariosvendidos = $this->getDoctrine()->getRepository("GranAdministradorBundle:EspaciosVendidos")->createQueryBuilder('ev')
            ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ev.idEspacio IN (:idEspacio)')->setParameter('idEspacio', $arrayIn)
            ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(1,2,3))
            ->andWhere('ev.fechaInicial <= :fechaInicial')->setParameter('fechaInicial', $now)
            ->andWhere('ev.fechaFinal >= :fechaFinal')->setParameter('fechaFinal', $now)
            ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId());
            $vendidosCount = 0;
            foreach($espaciospublicitariosvendidos->getQuery()->execute() as $k3 => $v3) {
                $vendidosCount ++;
            }
            $vendidosArray[$v1->getId()] = $vendidosCount;

            $usuariosEspacios = $em->getRepository('GranAdministradorBundle:EspaciosPublicitarios')->find($v1->getId());
            $contar = 0;           


            $disponiblesArray[$v1->getId()] = $solicitadosCount - $vendidosCount;

            $totalnegociados = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosVendidos")->createQueryBuilder('ev')
            ->select('COUNT(ev.id)')
            ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ev.idEspacio IN (:idEspacio)')->setParameter('idEspacio', $arrayIn)
            ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(3))
            ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId())
            ->getQuery()->getSingleScalarResult();

            $totalesArray[$v1->getId()] = $totalnegociados;
        }

        $usuariosExecute = $usuarios->orderBy("u.codigo","asc")->getQuery()->execute();
        foreach ($usuariosListExecute as $key => $value) {
            $xrow++;
            $activeSheet->setCellValue("A".$xrow , $value->getUsuario())->getStyle("A".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("B".$xrow , $value->getDrogueria())->getStyle("B".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("C".$xrow , $solicitadosArray[$value->getId()])->getStyle("C".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("D".$xrow , $vendidosArray[$value->getId()])->getStyle("D".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("E".$xrow , $disponiblesArray[$value->getId()])->getStyle("E".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("F".$xrow , $totalesArray[$value->getId()])->getStyle("F".$xrow)->getFont()->setBold(false);
        }        

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=EspaciosDisponibles.xls');
        header('Cache-Control: max-age=0');         
        $Excel_writer->save('php://output');
    }

    
    public function excel3Action(Request $request)
    {
        
        $useridObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $userid=$useridObj->getId();
        
        $em = $this->getDoctrine()->getManager(); 
        $data = $request->request->all();



        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();

        $xrow = 1;
        $activeSheet->setCellValue("A".$xrow , 'Nombre Espacio')->getStyle("A".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("B".$xrow , 'Droguería')->getStyle("B".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("C".$xrow , 'Estado')->getStyle("C".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("D".$xrow , 'Proveedor')->getStyle("D".$xrow)->getFont()->setBold(true);

        $activeSheet->setCellValue("E".$xrow , 'Contácto Nombre')->getStyle("E".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("F".$xrow , 'Contácto Cédula')->getStyle("F".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("G".$xrow , 'Contácto Celular')->getStyle("G".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("H".$xrow , 'Contácto Cargo')->getStyle("H".$xrow)->getFont()->setBold(true);

        $activeSheet->setCellValue("I".$xrow , 'Asignado por')->getStyle("I".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("J".$xrow , 'Fecha Asignación')->getStyle("J".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("k".$xrow , 'Fecha Inicio')->getStyle("L".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("L".$xrow , 'Fecha Fin')->getStyle("M".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("M".$xrow , 'Valor')->getStyle("N".$xrow)->getFont()->setBold(true);

        $activeSheet->getColumnDimension('A')->setAutoSize(true);
        $activeSheet->getColumnDimension('B')->setAutoSize(true);
        $activeSheet->getColumnDimension('C')->setAutoSize(true);
        $activeSheet->getColumnDimension('D')->setAutoSize(true);
        $activeSheet->getColumnDimension('E')->setAutoSize(true);
        $activeSheet->getColumnDimension('F')->setAutoSize(true);
        $activeSheet->getColumnDimension('G')->setAutoSize(true);
        $activeSheet->getColumnDimension('H')->setAutoSize(true);
        $activeSheet->getColumnDimension('I')->setAutoSize(true);
        $activeSheet->getColumnDimension('J')->setAutoSize(true);
        $activeSheet->getColumnDimension('K')->setAutoSize(true);
        $activeSheet->getColumnDimension('L')->setAutoSize(true);
        $activeSheet->getColumnDimension('M')->setAutoSize(true);
        

        $espaciosVendidos = $this->getDoctrine()->getRepository("GranAdministradorBundle:EspaciosVendidos")->createQueryBuilder('ev')
        ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
        ->leftJoin('GranAdministradorBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.id = ev.idEspacio')
        ->leftJoin('GranAdministradorBundle:Proveedores', 'p', 'WITH', 'p.id = ev.idProveedor')
        ->leftJoin('GranAdministradorBundle:Usuarios', 'u', 'WITH', 'u.id = ev.idUsuario');
        
        if(@$request->query->get('b') != ""){
           $espaciosVendidos->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('b')."%");
           $espaciosVendidos->orWhere('p.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('b')."%");
           $espaciosVendidos->orWhere('u.usuario LIKE :usuario1')->setParameter('usuario1', "%".$request->query->get('b')."%");
           $espaciosVendidos->orWhere('ev.proveedorNombre LIKE :proveedorNombre')->setParameter('proveedorNombre', "%".$request->query->get('b')."%");
        }

        $espaciosVendidosExecute = $espaciosVendidos->orderBy("ev.id","desc")->getQuery()->execute();

        foreach ($espaciosVendidosExecute as $key => $value) {
            $xrow++;
            if($value->getIdAdministradorSolicitud() != ""){
                $usuariott = "Administrador: ".$value->getIdAdministradorSolicitud()->getUsuario();
            }elseif($value->getIdKoalaSolicitud() != ""){
                $usuariott = "Operador Koala: ".$value->getIdKoalaSolicitud()->getUsuario();
            }elseif($value->getIdProveedorSolicitud() != ""){
                $usuariott = "Proveedor: ".$value->getIdProveedorSolicitud()->getUsuario();
            }else{
                $usuariott = "Droguería: ".$value->getIdUsuario()->getUsuario();
            }

            $activeSheet->setCellValue("A".$xrow , $value->getIdEspacio()->getIdTipoEspacio()->getNombre())->getStyle("A".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("B".$xrow , $value->getIdUsuario()->getUsuario())->getStyle("B".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("C".$xrow , $value->getIdEstado()->getEstado())->getStyle("C".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("D".$xrow , $value->getIdProveedor()->getNombre())->getStyle("D".$xrow)->getFont()->setBold(false);

            $activeSheet->setCellValue("E".$xrow , $value->getProveedorNombre())->getStyle("E".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("F".$xrow , $value->getProveedorCedula())->getStyle("F".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("G".$xrow , $value->getProveedorCelular())->getStyle("G".$xrow)->getFont()->setBold(false);

            $activeSheet->setCellValue("H".$xrow , $value->getProveedorCargo())->getStyle("H".$xrow)->getFont()->setBold(false);

            $activeSheet->setCellValue("I".$xrow , $usuariott)->getStyle("I".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("J".$xrow , $value->getFechaRadicacion()->format("Y-m-d H:i:s"))->getStyle("J".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("K".$xrow , $value->getFechaInicial()->format("Y-m-d"))->getStyle("K".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("L".$xrow , $value->getFechaFinal()->format("Y-m-d"))->getStyle("L".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("M".$xrow , $value->getValor())->getStyle("M".$xrow)->getFont()->setBold(false);

        }

        
        

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=EspaciosNegociados.xls');
        header('Cache-Control: max-age=0');         
        $Excel_writer->save('php://output');
    }
    
    
    public function excel4Action(Request $request)
    {
        $useridObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $userid=$useridObj->getId();
        
        $em = $this->getDoctrine()->getManager(); 
        $data = $request->request->all();



        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();

        $xrow = 1;
        $activeSheet->setCellValue("A".$xrow , 'Código')->getStyle("A".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("B".$xrow , 'Ciudad')->getStyle("B".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("C".$xrow , 'Regional')->getStyle("C".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("D".$xrow , 'Droguería')->getStyle("D".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("E".$xrow , 'Dirección')->getStyle("E".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("F".$xrow , 'Teléfono')->getStyle("F".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("G".$xrow , 'Nombre del Gerente')->getStyle("G".$xrow)->getFont()->setBold(true);

        $activeSheet->setCellValue("H".$xrow , 'Mostrador')->getStyle("H".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("I".$xrow , 'Autoservicio')->getStyle("I".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("J".$xrow , 'Entradas y Salidas')->getStyle("J".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("K".$xrow , 'Puntos de pago')->getStyle("K".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("L".$xrow , 'Número de Vendedores')->getStyle("L".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("M".$xrow , 'Área')->getStyle("M".$xrow)->getFont()->setBold(true);

        $activeSheet->getColumnDimension('A')->setAutoSize(true);
        $activeSheet->getColumnDimension('B')->setAutoSize(true);
        $activeSheet->getColumnDimension('C')->setAutoSize(true);
        $activeSheet->getColumnDimension('D')->setAutoSize(true);
        $activeSheet->getColumnDimension('E')->setAutoSize(true);
        $activeSheet->getColumnDimension('F')->setAutoSize(true);
        $activeSheet->getColumnDimension('G')->setAutoSize(true);
        $activeSheet->getColumnDimension('H')->setAutoSize(true);
        $activeSheet->getColumnDimension('I')->setAutoSize(true);
        $activeSheet->getColumnDimension('J')->setAutoSize(true);
        $activeSheet->getColumnDimension('K')->setAutoSize(true);
        $activeSheet->getColumnDimension('L')->setAutoSize(true);
        $activeSheet->getColumnDimension('M')->setAutoSize(true);


        $usuarios = $this->getDoctrine()->getRepository("GranAdministradorBundle:Usuarios")->createQueryBuilder('e')
        ->where('e.eliminado = :eliminado')->setParameter('eliminado', 0);
        
        if(@$request->query->get('b') != "" && @$request->query->get('tbvr') == "3"){

           $usuarios->andWhere('e.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('b')."%");           
           $usuarios->orWhere('e.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$request->query->get('b')."%");
           $usuarios->orWhere('e.codigo LIKE :codigo')->setParameter('codigo', "%".$request->query->get('b')."%");
           $usuarios->orWhere('e.drogueria LIKE :drogueria')->setParameter('drogueria', "%".$request->query->get('b')."%");
           
        }

        if(@$request->query->get('regional') != "Todas" && @$request->query->get('tbvr') == "1"){
           $usuarios->andWhere('e.regional = :regional')->setParameter('regional', $request->query->get('regional'));  

           if($request->query->get('ciudad') != "Todas"){
                $usuarios->andWhere('e.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$request->query->get('ciudad')."%");
            }           
        }

        if(@$request->query->get('idespacio') != "" && @$request->query->get('tbvr') == "2"){

            $usuarios->andWhere('ep.idTipoEspacio = :idTipoEspacio')->setParameter('idTipoEspacio', $request->query->get('idespacio'));
            $usuarios->leftJoin('GranAdministradorBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.idUsuario = e.id');           
        }

        $usuariosrun = $usuarios->orderBy("e.codigo","ASC")->getQuery()->execute();

        foreach ($usuariosrun as $key => $value) {
            $xrow++;
            $activeSheet->setCellValue("A".$xrow , $value->getUsuario())->getStyle("A".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("B".$xrow , $value->getCiudad())->getStyle("B".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("C".$xrow , $value->getRegional())->getStyle("C".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("D".$xrow , $value->getDrogueria())->getStyle("D".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("E".$xrow , $value->getDireccion())->getStyle("E".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("F".$xrow , $value->getTelefono())->getStyle("F".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("G".$xrow , $value->getNombreGerente())->getStyle("G".$xrow)->getFont()->setBold(false);

            $activeSheet->setCellValue("H".$xrow , $value->getMostrador())->getStyle("H".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("I".$xrow , $value->getAutoservicio())->getStyle("I".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("J".$xrow , $value->getEntradasSalidas())->getStyle("J".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("K".$xrow , $value->getPuntosPago())->getStyle("K".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("L".$xrow , $value->getNumeroVendedores())->getStyle("L".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("M".$xrow , $value->getArea())->getStyle("M".$xrow)->getFont()->setBold(false);

        }        

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=Usuarios.xls');
        header('Cache-Control: max-age=0');         
        $Excel_writer->save('php://output');
    }

    
    public function excel5Action(Request $request)
    {
        $useridObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $userid=$useridObj->getId();
        
        $em = $this->getDoctrine()->getManager(); 
        $data = $request->request->all();



        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();

        $xrow = 1;
        $activeSheet->setCellValue("A".$xrow , 'Usuario')->getStyle("A".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("B".$xrow , 'Contraseña')->getStyle("B".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("C".$xrow , 'Nombre')->getStyle("C".$xrow)->getFont()->setBold(true);
        $activeSheet->setCellValue("D".$xrow , 'Código')->getStyle("D".$xrow)->getFont()->setBold(true);
        

        $activeSheet->getColumnDimension('A')->setAutoSize(true);
        $activeSheet->getColumnDimension('B')->setAutoSize(true);
        $activeSheet->getColumnDimension('C')->setAutoSize(true);
        $activeSheet->getColumnDimension('D')->setAutoSize(true);
        

        $proveedores = $this->getDoctrine()->getRepository("GranAdministradorBundle:Proveedores")->createQueryBuilder('e');

        if(@$request->query->get('b') != ""){

           $proveedores->andWhere('e.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('b')."%");
           $proveedores->orWhere('e.nombre LIKE :nombre')->setParameter('nombre', "%".$request->query->get('b')."%");
           $proveedores->orWhere('e.codigo LIKE :codigo')->setParameter('codigo', "%".$request->query->get('b')."%");
           
        }
        $proveedores->andWhere('e.eliminado = :eliminado')->setParameter('eliminado', 0);
        $proveedoresExe = $proveedores->orderBy("e.usuario","asc")->getQuery()->execute();

        foreach ($proveedoresExe as $key => $value) {
            $xrow++;
            $activeSheet->setCellValue("A".$xrow , $value->getUsuario())->getStyle("A".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("B".$xrow , $value->getContrasena())->getStyle("B".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("C".$xrow , $value->getNombre())->getStyle("C".$xrow)->getFont()->setBold(false);
            $activeSheet->setCellValue("D".$xrow , $value->getCodigo())->getStyle("D".$xrow)->getFont()->setBold(false);            
        }        

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=Proveedores.xls');
        header('Cache-Control: max-age=0');         
        $Excel_writer->save('php://output');
    }
}
