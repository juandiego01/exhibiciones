<?php
// src/Gran/AdministradorBundle/Controller/CrearEspacioController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CrearEspacioController extends Controller
{    
    public function index(Request $request)
    {
        $retVar = "";
        
        return $this->render('Usuario/crearespacio/index.html.twig', array(
            'titulo' => "CrearEspacioAplicacion",
            'namevar' => "CrearEspacioAplicacion",
            'retvar' => $retVar,
                ));        
    }
    public function login(Request $request)
    {
        $retVar = "";
        
        return $this->render('Usuario/crearespacio/login.html.twig', array(
            'titulo' => "CrearEspacioAplicacion",
            'namevar' => "CrearEspacioAplicacion",
            'retvar' => $retVar,
                ));   
    }
    public function loginError(Request $request)
    {
        $retVar = "Error";
        
        return $this->render('Usuario/crearespacio/login.html.twig', array(
            'titulo' => "CrearEspacioAplicacion",
            'namevar' => "CrearEspacioAplicacion",
            'retvar' => $retVar,
                ));   
    }
}