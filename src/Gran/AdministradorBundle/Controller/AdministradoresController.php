<?php
// src/Gran/AdministradorBundle/Controller/AdministradoresController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\AdministradorBundle\Entity\Administradores;
use App\Gran\AdministradorBundle\Form\AdministradoresType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AdministradoresController extends Controller
{  
   public function indexAction(Request $request)
    {        
        $entityManager = $this->getDoctrine()->getManager();
        $postdata = $request->request->all();

        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $usuariodministrador=$objUsuario->getUsuario();
        if($usuariodministrador != "superadministrador"){
            return $this->redirectToRoute('administrador_inicio_logout');
        }

        $administradores = $this->getDoctrine()->getRepository("GranAdministradorBundle:Administradores")->createQueryBuilder('e')
        ->where('e.eliminado = :eliminado')->setParameter('eliminado', 0)->getQuery()->execute();

        return $this->render('Administrador/administradores/index.html.twig', array(
            'titulo' => "Administradores",
            'resultset' => $administradores,
        ));
    }    
   public function newAction(Request $request){
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $usuariodministrador=$objUsuario->getUsuario();
        if($usuariodministrador != "superadministrador"){
            return $this->redirectToRoute('administrador_inicio_logout');
        }

        $formvar = new Administradores();
        $form = $this->createForm("App\Gran\AdministradorBundle\Form\AdministradoresType", $formvar);        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {                    
            $entityManager = $this->getDoctrine()->getManager();
            $formvar->setFecha(new \DateTime(date("Y-m-d H:i:s")));
            $formvar->setEliminado(0);
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('administradores_admin_index');
        }
        return $this->render('Administrador/defaults/new.html.twig', array(            
            'form' => $form->createView(),
            'titulo' => "Administrador",
        ));
    }    
   public function editAction(Request $request, Administradores $formvar){
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $usuariodministrador=$objUsuario->getUsuario();
        if($usuariodministrador != "superadministrador"){
            return $this->redirectToRoute('administrador_inicio_logout');
        }

        $editForm = $this->createForm("App\Gran\AdministradorBundle\Form\AdministradoresType", $formvar);
        $editForm->handleRequest($request);
        $postdata = $request->request->all();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            if($formvar->getId() == 1){
                $formvar->setAccesoPermitido(1);
            }

            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('administradores_admin_index');
        }
        return $this->render('Administrador/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Administrador",
        ));
    }   
    public function deleteAction(Request $request, $id){   
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $usuariodministrador=$objUsuario->getUsuario();
        if($usuariodministrador != "superadministrador"){
            return $this->redirectToRoute('administrador_inicio_logout');
        }     
        $entityManager = $this->getDoctrine()->getManager();
        $delete = $entityManager->getRepository('GranAdministradorBundle:Administradores')->find($id);
        $delete->setEliminado(1);

        if($id == 1){
            $delete->setEliminado(0);
        }

        $entityManager->persist($delete);
        $entityManager->flush($delete);
        return $this->redirectToRoute('administradores_admin_index');
    } 
}
