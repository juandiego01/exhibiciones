<?php
// src/Gran/AdministradorBundle/Controller/InicioController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class InicioController extends Controller
{    
    public function index(Request $request){        
        return $this->render('Administrador/inicio/index.html.twig', array(
            'titulo' => "InicioAplicacion",
                ));        
    }
    public function login(Request $request){        
        return $this->render('Administrador/inicio/login.html.twig', array(
            'titulo' => "InicioAplicacion",
                ));   
    }
    public function loginError(Request $request){        
        return $this->render('Administrador/inicio/login.html.twig', array(
            'titulo' => "InicioAplicacion",
                ));   
    }
}