<?php
// src/Gran/AdministradorBundle/Controller/UsuarioskoalaController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\AdministradorBundle\Entity\Usuarioskoala;
use App\Gran\AdministradorBundle\Form\UsuarioskoalaType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class UsuarioskoalaController extends Controller
{  
   public function indexAction(Request $request)
    {        
        $userObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $entityManager = $this->getDoctrine()->getManager();
        $postdata = $request->request->all();

        $usuarioskoala = $this->getDoctrine()->getRepository("GranAdministradorBundle:Usuarioskoala")->createQueryBuilder('e')
        ->where('e.eliminado = :eliminado')->setParameter('eliminado', 0)->getQuery()->execute();

        return $this->render('Administrador/usuarioskoala/index.html.twig', array(
            'titulo' => "Operadores Koala",
            'resultset' => $usuarioskoala,
        ));
    }    
   public function newAction(Request $request){
        $formvar = new Usuarioskoala();
        $form = $this->createForm("App\Gran\AdministradorBundle\Form\UsuarioskoalaType", $formvar);        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {                    
            $entityManager = $this->getDoctrine()->getManager();
            $formvar->setFecha(new \DateTime(date("Y-m-d H:i:s")));
            $formvar->setEliminado(0);
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('usuarioskoala_admin_index');
        }
        return $this->render('Administrador/defaults/new.html.twig', array(            
            'form' => $form->createView(),
            'titulo' => "Operador Koala",
        ));
    }    
   public function editAction(Request $request, Usuarioskoala $formvar){
        $editForm = $this->createForm("App\Gran\AdministradorBundle\Form\UsuarioskoalaType", $formvar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('usuarioskoala_admin_index');
        }
        return $this->render('Administrador/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Operador Koala",
        ));
    }   
    public function deleteAction(Request $request, $id){        
        $entityManager = $this->getDoctrine()->getManager();
        $delete = $entityManager->getRepository('GranAdministradorBundle:Usuarioskoala')->find($id);
        $delete->setEliminado(1);
        $entityManager->persist($delete);
        $entityManager->flush($delete);
        return $this->redirectToRoute('usuarioskoala_admin_index');
    } 
}
