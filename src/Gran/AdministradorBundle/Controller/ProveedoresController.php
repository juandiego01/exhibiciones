<?php
// src/Gran/AdministradorBundle/Controller/ProveedoresController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\AdministradorBundle\Entity\Proveedores;
use App\Gran\AdministradorBundle\Form\ProveedoresType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ProveedoresController extends Controller
{  
   public function indexAction(Request $request)
    {        
        $userObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $entityManager = $this->getDoctrine()->getManager();
        $postdata = $request->request->all();

        $proveedores = $this->getDoctrine()->getRepository("GranAdministradorBundle:Proveedores")->createQueryBuilder('e');

        if(@$request->query->get('buscar') != ""){

           $proveedores->andWhere('e.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");
           $proveedores->orWhere('e.nombre LIKE :nombre')->setParameter('nombre', "%".$request->query->get('buscar')."%");
           $proveedores->orWhere('e.codigo LIKE :codigo')->setParameter('codigo', "%".$request->query->get('buscar')."%");
           
        }
        $proveedores->andWhere('e.eliminado = :eliminado')->setParameter('eliminado', 0);
        $proveedoresExe = $proveedores->orderBy("e.usuario","asc")->getQuery()->execute();

        return $this->render('Administrador/proveedores/index.html.twig', array(
            'titulo' => "Proveedores",
            'buscar' => @$request->query->get('buscar'),
            'resultset' => $proveedoresExe,
        ));
    }    
   public function newAction(Request $request){
        $formvar = new Proveedores();
        $form = $this->createForm("App\Gran\AdministradorBundle\Form\ProveedoresType", $formvar);        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {                    
            $entityManager = $this->getDoctrine()->getManager();
            $formvar->setFecha(new \DateTime(date("Y-m-d H:i:s")));
            $formvar->setEliminado(0);
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('proveedores_admin_index');
        }
        return $this->render('Administrador/defaults/new.html.twig', array(            
            'form' => $form->createView(),
            'titulo' => "Proveedor",
        ));
    }    
   public function editAction(Request $request, Proveedores $formvar){
        $editForm = $this->createForm("App\Gran\AdministradorBundle\Form\ProveedoresType", $formvar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('proveedores_admin_index');
        }
        return $this->render('Administrador/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Proveedor",
        ));
    }   
    public function deleteAction(Request $request, $id){        
        $entityManager = $this->getDoctrine()->getManager();
        $delete = $entityManager->getRepository('GranAdministradorBundle:Proveedores')->find($id);
        $delete->setEliminado(1);
        $entityManager->persist($delete);
        $entityManager->flush($delete);
        return $this->redirectToRoute('proveedores_admin_index');
    } 
}
