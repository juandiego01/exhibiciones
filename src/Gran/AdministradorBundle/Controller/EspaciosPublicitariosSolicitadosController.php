<?php
// src/Gran/AdministradorBundle/Controller/EspaciosPublicitariosSolicitadosController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\AdministradorBundle\Entity\EspaciosPublicitarios;
use App\Gran\AdministradorBundle\Form\EspaciosPublicitariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Cpdg\UsuarioBundle\Controller\GlobalesController;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class EspaciosPublicitariosSolicitadosController extends Controller
{    
    public function indexAction(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciospublicitarios = $this->getDoctrine()->getRepository("GranAdministradorBundle:EspaciosPublicitarios")->createQueryBuilder('ep')        
        ->leftJoin('GranAdministradorBundle:Usuarios', 'u', 'WITH', 'u.id = ep.idUsuario')
        ->leftJoin('GranAdministradorBundle:EspaciosPublicitariosTipos', 'ept', 'WITH', 'ept.id = ep.idTipo')
        ->leftJoin('GranAdministradorBundle:EspaciosPublicitariosEstados', 'epe', 'WITH', 'epe.id = ep.idEstado')
        ;
        
        if(@$request->query->get('buscar') != ""){
           $espaciospublicitarios->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");           
           $espaciospublicitarios->orWhere('ep.descripcion LIKE :descripcion')->setParameter('descripcion', "%".$request->query->get('buscar')."%");
           $espaciospublicitarios->orWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");  
           $espaciospublicitarios->orWhere('epe.estado LIKE :estado')->setParameter('estado', "%".$request->query->get('buscar')."%");          
        }

        $espaciospublicitarios->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
                              ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 2);

        $espaciospublicitariosExecute = $espaciospublicitarios->orderBy("ep.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciospublicitariosExecute,
            $pagev,$cantv
        );

        return $this->render('Administrador/espaciospublicitariossolicitados/index.html.twig', array(
            'titulo' => "Espacios Solicitados",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'buscar' => @$request->query->get('buscar'),            
                ));        
    }   

    public function editAction(Request $request, EspaciosPublicitarios $formvar){
        $editForm = $this->createForm("App\Gran\AdministradorBundle\Form\EspaciosPublicitariosSolicitadosType", $formvar);
        $editForm->handleRequest($request);

        $ArchivoDir = $this->container->getParameter('kernel.root_dir').'/../public/images/espaciospublicitarios/';
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('espaciospublicitariossolicitados_admin_inicio');
        }

        return $this->render('Administrador/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Espacio Publicitario",
            'crear_espacio_usuario' => "true", 
        ));
    }  
}