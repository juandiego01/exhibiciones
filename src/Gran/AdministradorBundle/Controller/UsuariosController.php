<?php
// src/Gran/AdministradorBundle/Controller/UsuariosController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\AdministradorBundle\Entity\Usuarios;
use App\Gran\AdministradorBundle\Entity\UsuariosEspacios;
use App\Gran\AdministradorBundle\Form\UsuariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class UsuariosController extends Controller
{  
   public function indexAction(Request $request)
    {        
        $userObj = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $entityManager = $this->getDoctrine()->getManager();
        $postdata = $request->request->all();
        $t = 0;               

        $usuarios = $this->getDoctrine()->getRepository("GranAdministradorBundle:Usuarios")->createQueryBuilder('e')
        ->where('e.eliminado = :eliminado')->setParameter('eliminado', 0);
        
        if(@$request->query->get('buscar') != "" && @$request->query->get('tbvr') == "3"){

           $usuarios->andWhere('e.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");           
           $usuarios->orWhere('e.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$request->query->get('buscar')."%");
           $usuarios->orWhere('e.codigo LIKE :codigo')->setParameter('codigo', "%".$request->query->get('buscar')."%");
           $usuarios->orWhere('e.drogueria LIKE :drogueria')->setParameter('drogueria', "%".$request->query->get('buscar')."%");
           
        }

        if(@$request->query->get('regional') != "Todas" && @$request->query->get('tbvr') == "1"){
           $usuarios->andWhere('e.regional = :regional')->setParameter('regional', $request->query->get('regional'));  
        }
        if(@$request->query->get('ciudad') != "Todas" && @$request->query->get('tbvr') == "1"){
            $usuarios->andWhere('e.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$request->query->get('ciudad')."%");
        }

        if(@$request->query->get('idespacio') != "" && @$request->query->get('tbvr') == "2"){

            $usuarios->andWhere('ep.idTipoEspacio = :idTipoEspacio')->setParameter('idTipoEspacio', $request->query->get('idespacio'));
            $usuarios->leftJoin('GranAdministradorBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.idUsuario = e.id');           
        }

        $usuariosrun = $usuarios->orderBy("e.codigo","ASC")->getQuery()->execute();


        $espaciospublicitariostipos = $this->getDoctrine()->getRepository("GranAdministradorBundle:UsuariosEspaciosTipos")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->getQuery()->execute();

        $ciudadesRegionales = $this->getDoctrine()->getRepository("GranAdministradorBundle:Usuarios")->createQueryBuilder('u')
        ->groupBy("u.ciudad")->orderBy("u.ciudad","asc")->getQuery()->execute();
        $arrCiudadesRegionales = array();
        $arrCiudadesAll = array();
        foreach ($ciudadesRegionales as $fila) {
            $arrCiudadesRegionales[$fila->getRegional()][] = $fila->getCiudad();
            $arrCiudadesAll[] = $fila->getCiudad();
        }

        return $this->render('Administrador/usuarios/index.html.twig', array(
            'titulo' => "Droguerías",
            'resultset' => $usuariosrun,
            'arrCiudadesRegionales' => $arrCiudadesRegionales,
            'arrCiudadesAll' => $arrCiudadesAll,
            'espaciospublicitariostipos' => $espaciospublicitariostipos,
            't' => $t,
            'buscar' => @$request->query->get('buscar'),
            'errorvar' => @$request->query->get('errorvar'),
            'idespacio' => @$request->query->get('idespacio'),
            'regional' => @$request->query->get('regional'),
            'ciudad' => @$request->query->get('ciudad'),
            'tbvr' => @$request->query->get('tbvr'),
        ));
    }    
   public function newAction(Request $request){
        $formvar = new Usuarios();
        $form = $this->createForm("App\Gran\AdministradorBundle\Form\UsuariosType", $formvar);        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {                    
            $entityManager = $this->getDoctrine()->getManager();
            $verrorvar = "";
            try{
                $formvar->setFecha(new \DateTime(date("Y-m-d H:i:s")));
                $formvar->setEliminado(0);
                $entityManager->persist($formvar);
                $entityManager->flush($formvar);

                $usuariosEspacios = new UsuariosEspacios();

                $usuarioObj = $entityManager->getReference('GranAdministradorBundle:Usuarios', $formvar->getId());

                $usuariosEspacios->setIdUsuario($usuarioObj);
                $entityManager->persist($usuariosEspacios);
                $entityManager->flush($usuariosEspacios);

            } catch(\Doctrine\DBAL\DBALException $e) {
                $verrorvar = $formvar->getCodigo();
            }


            return $this->redirectToRoute('usuarios_admin_index', array("errorvar"=>$verrorvar));
        }
        return $this->render('Administrador/defaults/new.html.twig', array(            
            'form' => $form->createView(),
            'titulo' => "Droguería",
        ));
    }    
   public function editAction(Request $request, Usuarios $formvar){
        $editForm = $this->createForm("App\Gran\AdministradorBundle\Form\UsuariosType", $formvar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('usuarios_admin_index');
        }
        return $this->render('Administrador/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),
            'titulo' => "Droguería",
        ));
    }   
    public function deleteAction(Request $request, $id){        
        $entityManager = $this->getDoctrine()->getManager();
        $delete = $entityManager->getRepository('GranAdministradorBundle:Usuarios')->find($id);
        $delete->setEliminado(1);
        $entityManager->persist($delete);
        $entityManager->flush($delete);
        return $this->redirectToRoute('usuarios_admin_index');
    } 
}
