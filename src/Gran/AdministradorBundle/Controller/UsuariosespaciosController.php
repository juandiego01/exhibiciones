<?php
// src/Gran/AdministradorBundle/Controller/UsuariosespaciosController.php
namespace App\Gran\AdministradorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\AdministradorBundle\Entity\Usuarios;
use App\Gran\AdministradorBundle\Form\UsuariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class UsuariosespaciosController extends Controller
{  
   public function indexAction(Request $request)
    {        
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idUsuario=$objUsuario->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $postdata = $request->request->all();

        $usuarios = $this->getDoctrine()->getRepository("GranAdministradorBundle:UsuariosEspacios")->createQueryBuilder('e')
        ->getQuery()->execute();

        return $this->render('Administrador/usuariosespacios/index.html.twig', array(
            'titulo' => "Droguerías",
            'resultset' => $usuarios,
        ));
    }        
   public function editAction(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idUsuario=$objUsuario->getId();

        $usuariosespacios = $entityManager->getRepository('GranAdministradorBundle:UsuariosEspacios')->find($id);

        $espaciospublicitariostipos = $this->getDoctrine()->getRepository("GranUsuarioBundle:UsuariosEspaciosTipos")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->getQuery()->execute();

        $espaciospublicitariosdisponiblesq = $this->getDoctrine()->getRepository("GranUsuarioBundle:UsuariosEspacios")->createQueryBuilder('ep')
            ->where('ep.idUsuario = :idUsuario')->setParameter('idUsuario', $id)
            ->getQuery()->execute();
            
        $espaciospublicitariosdisponibles = array();

        foreach ($espaciospublicitariosdisponiblesq as $value) {
            $espaciospublicitariosdisponibles[1] = $value->getDisponible1();
            $espaciospublicitariosdisponibles[2] = $value->getDisponible2();
            $espaciospublicitariosdisponibles[3] = $value->getDisponible3();
            $espaciospublicitariosdisponibles[4] = $value->getDisponible4();
            $espaciospublicitariosdisponibles[5] = $value->getDisponible6();
            $espaciospublicitariosdisponibles[6] = $value->getDisponible6();
            $espaciospublicitariosdisponibles[7] = $value->getDisponible7();
            $espaciospublicitariosdisponibles[8] = $value->getDisponible8();
            $espaciospublicitariosdisponibles[9] = $value->getDisponible9();
            $espaciospublicitariosdisponibles[10] = $value->getDisponible10();
            $espaciospublicitariosdisponibles[11] = $value->getDisponible11();
            $espaciospublicitariosdisponibles[12] = $value->getDisponible12();
            $espaciospublicitariosdisponibles[13] = $value->getDisponible13();
        }

        return $this->render('Administrador/usuariosespacios/ver.html.twig', array(
            'titulo' => "Espacios de Droguería",
            'usuariosespacios' => $usuariosespacios,
            'espaciospublicitariosdisponibles' => $espaciospublicitariosdisponibles,
            'espaciospublicitariostipos' => $espaciospublicitariostipos,
        ));
    }   
    public function deleteAction(Request $request, $id){        
        $entityManager = $this->getDoctrine()->getManager();
        $delete = $entityManager->getRepository('GranAdministradorBundle:Usuarios')->find($id);
        $delete->setEliminado(1);
        $entityManager->persist($delete);
        $entityManager->flush($delete);
        return $this->redirectToRoute('usuariosespacios_admin_index');
    } 
}
