<?php

namespace App\Gran\AdministradorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EspaciosVendidosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        
            ->add('idProveedor', EntityType::class, array(
                    'class' => 'GranAdministradorBundle:Proveedores',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                            ->orderBy('e.nombre', 'ASC');
                    },
                    'choice_label' => function ($bodegas) {
                        return $bodegas->getNombre();
                    },
                    'label' => "Proveedor: ",                    
                ))
            ->add('ValorMensual', TextType::class, array(
                'data' => "0",
                'required' => true,
                'mapped' => false,
                'label' => "Valor Mensual: ",
                'attr' => array('onkeyup' => 'recaulcular()', 'min' => '0', 'onkeypress' => 'return solonumeros(event)',),
            )) 
            ->add('dias', TextType::class, array(
                'data' => "0",
                'required' => true,
                'mapped' => false,
                'label' => "Días: ",
                'attr' => array('readonly' => 'readonly', ),
            ))
            ->add('Valor', TextType::class, array(
                'required' => true,
                'data' => "0",
                'label' => "Valor Total: ",
                'attr' => array('readonly' => 'readonly'),
            ))              

            ->add('fotoEspacio', FileType::class, array(
                'required' => false,
                'label'    => 'Adjuntar foto del espacio, La imágen debe estar en formato jpg',                
                'attr' => array('onchange' => 'validateimg(this)'),
                'mapped' => true,
            ))

            ->add('fotoMaterial', FileType::class, array(
                'required' => false,
                'label'    => 'Adjuntar foto del Material Publicitario, La imágen debe estar en formato jpg',                
                'attr' => array('onchange' => 'validateimg(this)'),
                'mapped' => true,
            ))

            ->add('proveedorNombre', TextType::class, array(
                'required' => true,
                'label' => "Nombre del contacto del proveedor: ",
            ))
            ->add('proveedorCargo', TextType::class, array(
                'required' => true,
                'label' => "Cargo del contacto del proveedor: ",
            ))
            ->add('proveedorCedula', NumberType::class, array(
                'required' => true,
                'label' => "Cédula del contacto del proveedor: ",
            ))
            ->add('proveedorCelular', TextType::class, array(
                'required' => true,
                'label' => "Celular del contacto del proveedor: ",
            ))            
        ;
    }    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Gran\AdministradorBundle\Entity\EspaciosVendidos'
        ));
    }
}
