<?php

namespace App\Gran\AdministradorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class UsuariosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('usuario', TextType::class, array(
                'required' => true,
                'label' => "Usuario: ",
            ))            

            ->add('ciudad', TextType::class, array(
                'required' => true,
                'label' => "Ciudad: ",
            ))    

            ->add('regional', TextType::class, array(
                'required' => true,
                'label' => "Regional: ",
            ))  

            ->add('regional', ChoiceType::class, array(
                'choices' => array(
                    'PEREIRA' => '2100',
                    'BARRANQUILLA' => '3100',
                    'MEDELLIN' => '5100',
                    'CALI' => '6100',
                    'COTA' => '8100',
                    'BUCARAMANGA' => '9100',
                ),
                'label'    => 'Regional',
                'required'    => true,
                'empty_data'  => null
            )) 

            ->add('codigo', TextType::class, array(
                'required' => true,
                'label' => "Código: ",
            ))
            ->add('drogueria', TextType::class, array(
                'required' => true,
                'label' => "Nombre Droguería: ",
            ))  

            ->add('direccion', TextType::class, array(
                'required' => true,
                'label' => "Dirección: ",
            ))  

            ->add('telefono', TextType::class, array(
                'required' => true,
                'label' => "Teléfono: ",
            ))   

            ->add('nombreGerente', TextType::class, array(
                'required' => true,
                'label' => "Nombre Gerente: ",
            )) 

            ->add('mostrador', ChoiceType::class, array(
                'choices' => array(
                    'Si' => 'X',
                    'No' => '0'
                ),
                'label'    => '¿Mostrador?',
                'required'    => true,
                'empty_data'  => null
            ))  

            ->add('autoservicio', ChoiceType::class, array(
                'choices' => array(
                    'Si' => 'X',
                    'No' => '0'
                ),
                'label'    => '¿Autoservicio?',
                'required'    => true,
                'empty_data'  => null
            ))         
            
            ->add('entradasSalidas', TextType::class, array(
                'required' => true,
                'label' => "Cantidad de entradas y salidas: ",
            )) 

            ->add('puntosPago', TextType::class, array(
                'required' => true,
                'label' => "Cantidad de puntos de pago: ",
            )) 

            ->add('numeroVendedores', TextType::class, array(
                'required' => true,
                'label' => "Número de vendedores: ",
            )) 

            ->add('area', TextType::class, array(
                'required' => true,
                'label' => "Área: ",
            ))          
            ->add('accesoPermitido', ChoiceType::class, array(
                'choices' => array(
                    'Si' => '1',
                    'No' => '0'
                ),
                'label'    => '¿Acceso Permitido?',
                'required'    => true,
                'empty_data'  => null
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Gran\AdministradorBundle\Entity\Usuarios'
        ));
    }
}
