<?php

namespace App\Gran\AdministradorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EspaciosPublicitariosEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder                      
            ->add('nombreEspacio', TextType::class, array(
                'required' => true,
                'label' => "Nombre: ",
            ))
            ->add('descripcion', TextareaType::class, array(
                'required' => true,
                'label' => "Descripción: ",
            ))
            ->add('alto', IntegerType::class, array(
                'required' => true,
                'label' => "Alto: ",
            ))
            ->add('ancho', IntegerType::class, array(
                'required' => true,
                'label' => "Ancho: ",
            ))
            ->add('foto', FileType::class, array(
                'required' => false,
                'label'    => 'Adjuntar foto del espacio, La imágen debe estar en formato jpg',                
                'attr' => array('onchange' => 'validateimg(this)'),                
                'data' => '',
            ))
            ->add('publico', ChoiceType::class, array(
                'choices' => array(
                    'Si' => '1',
                    'No' => '0'
                ),
                'label'    => '¿Público?',
                'required'    => true,
                'empty_data'  => null
            ))    
        ;
    }    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Gran\AdministradorBundle\Entity\EspaciosPublicitarios'
        ));
    }
}
