<?php

namespace App\Gran\AdministradorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsuariosEspaciosTipos
 *
 * @ORM\Table(name="usuarios_espacios_tipos")
 * @ORM\Entity
 */
class UsuariosEspaciosTipos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=false)
     */
    private $eliminado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEliminado(): ?bool
    {
        return $this->eliminado;
    }

    public function setEliminado(bool $eliminado): self
    {
        $this->eliminado = $eliminado;

        return $this;
    }


}
