<?php

namespace App\Gran\KoalaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EspaciosPublicitariosSolicitadosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder 
            ->add('idEstado', EntityType::class, array(
                    'class' => 'GranKoalaBundle:EspaciosPublicitariosEstados',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                            ->orderBy('e.estado', 'ASC');
                    },
                    'choice_label' => function ($bodegas) {
                        return $bodegas->getEstado();
                    },
                    'label' => "Estado: ",
                ))                                             

            ->add('publico', ChoiceType::class, array(
                'choices' => array(
                    'Si' => '1',
                    'No' => '0'
                ),
                'label'    => '¿Público?',
                'required'    => true,
                'empty_data'  => null
            ))    
        ;
    }    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Gran\KoalaBundle\Entity\EspaciosPublicitarios'
        ));
    }
}
