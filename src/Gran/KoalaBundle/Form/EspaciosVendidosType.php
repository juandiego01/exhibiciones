<?php

namespace App\Gran\KoalaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EspaciosVendidosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ValorMensual', TextType::class, array(
                'data' => "0",
                'required' => true,
                'mapped' => false,
                'label' => "Valor Mensual: ",
                'attr' => array('onkeyup' => 'recaulcular()', 'min' => '0', ),
            )) 

            ->add('dias', IntegerType::class, array(
                'data' => "0",
                'required' => true,
                'mapped' => false,
                'label' => "Días: ",
                'attr' => array('readonly' => 'readonly', ),
            ))

            ->add('Valor', TextType::class, array(
                'required' => true,
                'data' => "0",
                'label' => "Valor Total: ",
                'attr' => array('readonly' => 'readonly'),
            ))              

            ->add('fotoEspacio', FileType::class, array(
                'required' => false,
                'label'    => 'Adjuntar foto del espacio, La imágen debe estar en formato jpg',                
                'attr' => array('onchange' => 'validateimg(this)'),
                'mapped' => true,
            ))

            ->add('fotoMaterial', FileType::class, array(
                'required' => false,
                'label'    => 'Adjuntar foto del Material Publicitario, La imágen debe estar en formato jpg',                
                'attr' => array('onchange' => 'validateimg(this)'),
                'mapped' => true,
            ))

            ->add('proveedorNombre', TextType::class, array(
                'required' => true,
                'label' => "Nombre del contacto Koala: ",
            ))
            ->add('proveedorCargo', TextType::class, array(
                'required' => true,
                'label' => "Cargo del contacto Koala: ",
            ))
            ->add('proveedorCedula', IntegerType::class, array(
                'required' => true,
                'label' => "Cédula del contacto Koala: ",
            ))
            ->add('proveedorCelular', IntegerType::class, array(
                'required' => true,
                'label' => "Celular del contacto Koala: ",
            ))            
        ;
    }    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Gran\KoalaBundle\Entity\EspaciosVendidos'
        ));
    }
}
