<?php
// src/Gran/KoalaBundle/Controller/EspaciosPublicitariosSolicitadosController.php
namespace App\Gran\KoalaBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\KoalaBundle\Entity\EspaciosPublicitarios;
use App\Gran\KoalaBundle\Form\EspaciosPublicitariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Cpdg\UsuarioBundle\Controller\GlobalesController;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class EspaciosPublicitariosSolicitadosController extends Controller
{    
    public function indexAction(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciospublicitarios = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
        ;


        if(@$request->query->get('buscar') != ""){
           $espaciospublicitarios->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");
           $espaciospublicitarios->orWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");
           $espaciospublicitarios->orWhere('u.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$request->query->get('buscar')."%");

           $espaciospublicitarios->orWhere('u.drogueria LIKE :drogueria')->setParameter('drogueria', "%".$request->query->get('buscar')."%");

        }

        if(@$request->query->get('regional') != "" && @$request->query->get('tbvr') == "1"){
           $espaciospublicitarios->andWhere('u.regional = :regional')->setParameter('regional', $request->query->get('regional'));           
           if($request->query->get('ciudad') != "Todas"){
                $espaciospublicitarios->andWhere('u.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$request->query->get('ciudad')."%");
            } 
        }

        if(@$request->query->get('idespacio') != "" && @$request->query->get('tbvr') == "2"){
           $espaciospublicitarios->andWhere('uet.id = :id1')->setParameter('id1', $request->query->get('idespacio')); 

        }

        $espaciospublicitarios->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
                              ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 2);

        $espaciospublicitariosExecute = $espaciospublicitarios
        ->leftJoin('GranKoalaBundle:UsuariosEspaciosTipos', 'uet', 'WITH', 'uet.id = ep.idTipoEspacio')
        ->leftJoin('GranKoalaBundle:Usuarios', 'u', 'WITH', 'u.id = ep.idUsuario')
        ->orderBy("ep.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciospublicitariosExecute,
            $pagev,$cantv
        );

        $espaciospublicitariostipos = $this->getDoctrine()->getRepository("GranKoalaBundle:UsuariosEspaciosTipos")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->getQuery()->execute();

            $ciudadesRegionales = $this->getDoctrine()->getRepository("GranKoalaBundle:Usuarios")->createQueryBuilder('u')
        ->groupBy("u.ciudad")->getQuery()->execute();
        $arrCiudadesRegionales = array();
        foreach ($ciudadesRegionales as $fila) {
            $arrCiudadesRegionales[$fila->getRegional()][] = $fila->getCiudad();
        }

        return $this->render('Koala/espaciospublicitariossolicitados/index.html.twig', array(
            'titulo' => "Espacios Solicitados",
            'resultset' => $resultset,
            'arrCiudadesRegionales' => $arrCiudadesRegionales,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'espaciospublicitariostipos' => $espaciospublicitariostipos,         
            'buscar' => @$request->query->get('buscar'),
            'errorvar' => @$request->query->get('errorvar'),
            'idespacio' => @$request->query->get('idespacio'),
            'regional' => @$request->query->get('regional'),             
                ));        
    }   

    public function editAction(Request $request, EspaciosPublicitarios $formvar){
        $editForm = $this->createForm("App\Gran\KoalaBundle\Form\EspaciosPublicitariosSolicitadosType", $formvar);
        $editForm->handleRequest($request);

        $ArchivoDir = $this->container->getParameter('kernel.root_dir').'/../public/images/espaciospublicitarios/';
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('espaciospublicitariossolicitados_koala_inicio');
        }

        return $this->render('Koala/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Espacio Publicitario",
        ));
    }  
}