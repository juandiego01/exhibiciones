<?php
// src/Gran/KoalaBundle/Controller/EspaciosDisponiblesController.php
namespace App\Gran\KoalaBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\KoalaBundle\Entity\EspaciosPublicitarios;
use App\Gran\KoalaBundle\Form\EspaciosPublicitariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Cpdg\UsuarioBundle\Controller\GlobalesController;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class EspaciosDisponiblesController extends Controller
{    
    public function indexAction(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');
        $entityManager = $this->getDoctrine()->getManager();

        
        $idsArrayBase = array();
        $espaciospublicitariosdefinidos = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 1);
        $arrayDefinidos = array();
        $predefinidos = "";
        $predefinidosCount = 0;
        foreach ($espaciospublicitariosdefinidos->getQuery()->execute() as $k => $v) {
            $arrayDefinidos[$v->getId()]["id"] = $v->getId();
            $arrayDefinidos[$v->getId()]["nombre"] = $v->getNombreEspacio();
            $predefinidos .= $v->getNombreEspacio().", ";
            $predefinidosCount++;
            $idsArrayBase[] = $v->getId();
        }
        $predefinidos = substr($predefinidos, 0, -2);        
        
        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $usuariosList = $this->getDoctrine()->getRepository("GranKoalaBundle:Usuarios")->createQueryBuilder('u')
        ->andWhere('u.eliminado = :eliminado')->setParameter('eliminado', 0);

        $usuarios = $this->getDoctrine()->getRepository("GranKoalaBundle:Usuarios")->createQueryBuilder('u');
        
        if(@$request->query->get('buscar') != ""){
           $usuarios->andWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%")
           ->orWhere('u.drogueria LIKE :drogueria')->setParameter('drogueria', "%".$request->query->get('buscar')."%");  

           $usuariosList->andWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%")
           ->orWhere('u.drogueria LIKE :drogueria')->setParameter('drogueria', "%".$request->query->get('buscar')."%");        
        }

        $usuarios->andWhere('u.eliminado = :eliminado')->setParameter('eliminado', 0);

        $solicitadosArray = array(); 
        $vendidosArray = array(); 
        $disponiblesArray = array(); 
        $disponiblesArray = array(); 
        $totalesArray = array(); 


        $usuariosListExecute = $usuariosList->orderBy("u.codigo","asc")->getQuery()->execute();
        $resultsetList = $paginator->paginate(
            $usuariosListExecute,
            $pagev,$cantv
        );        
        foreach ($resultsetList as $k1 => $v1) {
            $solicitados = "";
            $solicitadosCount = 0;
            $idsArray = array();
            $espaciospublicitariossoliciados = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ep.idEstado = :idEstado')->setParameter('idEstado', 2)
            ->andWhere('ep.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId());
            
            foreach ($espaciospublicitariossoliciados->getQuery()->execute() as $k2 => $v2){
                $solicitados .= $v2->getNombreEspacio().", ";
                $solicitadosCount++;
                $idsArray[$v2->getId()] = $v2->getId();
            }
            $solicitados = substr($solicitados, 0, -2);            
            $solicitadosArray[$v1->getId()]["solicitados"] = $solicitadosCount;

            $arrayIn = array_merge($idsArrayBase, $idsArray);
            $now = date("Y-m-d");
            $espaciospublicitariosvendidos = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosVendidos")->createQueryBuilder('ev')
            ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ev.idEspacio IN (:idEspacio)')->setParameter('idEspacio', $arrayIn)
            ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(1,2,3))
            ->andWhere('ev.fechaInicial <= :fechaInicial')->setParameter('fechaInicial', $now)
            ->andWhere('ev.fechaFinal >= :fechaFinal')->setParameter('fechaFinal', $now)
            ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId());
            $vendidosCount = 0;
            foreach($espaciospublicitariosvendidos->getQuery()->execute() as $k3 => $v3) {
                $vendidosCount ++;
            }
            $vendidosArray[$v1->getId()] = $vendidosCount;

            $usuariosEspacios = $entityManager->getRepository('GranKoalaBundle:EspaciosPublicitarios')->find($v1->getId());
            $contar = 0;           


            $disponiblesArray[$v1->getId()] = $solicitadosCount - $vendidosCount;

            $totalnegociados = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosVendidos")->createQueryBuilder('ev')
            ->select('COUNT(ev.id)')
            ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ev.idEspacio IN (:idEspacio)')->setParameter('idEspacio', $arrayIn)
            ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(3))
            ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId())
            ->getQuery()->getSingleScalarResult();

            $totalesArray[$v1->getId()] = $totalnegociados;
        }


        
        $usuariosExecute = $usuarios->orderBy("u.codigo","asc")->getQuery()->execute();

        $resultset = $paginator->paginate(
            $usuariosExecute,
            $pagev,$cantv
        );

        return $this->render('Koala/espaciosdisponibles/index.html.twig', array(
            'titulo' => "Espacios Publicitarios Disponibles",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'predefinidos' => $predefinidosCount,            
            'solicitados' => $solicitadosArray,            
            'vendidos' => $vendidosArray,  
            'disponibles' => $disponiblesArray,  
            'totales' => $totalesArray,  
            'buscar' => @$request->query->get('buscar'),            
                ));        
    }
    
}