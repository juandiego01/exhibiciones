<?php
// src/Gran/KoalaBundle/Controller/EspaciosPublicitariosController.php
namespace App\Gran\KoalaBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\KoalaBundle\Entity\EspaciosPublicitarios;
use App\Gran\KoalaBundle\Form\EspaciosPublicitariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Cpdg\UsuarioBundle\Controller\GlobalesController;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class EspaciosPublicitariosController extends Controller
{    
    public function indexAction(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciospublicitarios = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosPublicitarios")->createQueryBuilder('ep')        
        ->leftJoin('GranKoalaBundle:Administradores', 'a', 'WITH', 'a.id = ep.idAdministrador')
        ->leftJoin('GranKoalaBundle:EspaciosPublicitariosTipos', 'ept', 'WITH', 'ept.id = ep.idTipo');
        
        if(@$request->query->get('buscar') != ""){
           $espaciospublicitarios->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");           
           $espaciospublicitarios->orWhere('ep.descripcion LIKE :descripcion')->setParameter('descripcion', "%".$request->query->get('buscar')."%");
           $espaciospublicitarios->orWhere('a.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");           
        }

        $espaciospublicitarios->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
                              ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 1);

        $espaciospublicitariosExecute = $espaciospublicitarios->orderBy("ep.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciospublicitariosExecute,
            $pagev,$cantv
        );

        return $this->render('Koala/espaciospublicitarios/index.html.twig', array(
            'titulo' => "Tipos de espacios",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'buscar' => @$request->query->get('buscar'),            
                ));        
    }
    public function newAction(Request $request){
        $formvar = new EspaciosPublicitarios();
        $form = $this->createForm("App\Gran\KoalaBundle\Form\EspaciosPublicitariosType", $formvar);        
        $form->handleRequest($request);
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idAdministrador=$objUsuario->getId();

        $ArchivoDir = $this->container->getParameter('kernel.root_dir').'/../public/images/espaciospublicitarios/';
        if ($form->isSubmitted() && $form->isValid()) {                    
            $entityManager = $this->getDoctrine()->getManager();
            $postdata = $request->request->all();

            $formvar->setFechaCreacion(new \DateTime(date("Y-m-d H:i:s")));
            $formvar->setEliminado(0);
            
             $fileName = md5(uniqid()).'.jpg';             
             $imagen = $formvar->getFoto();
             if($imagen != ""){
                copy($imagen,$ArchivoDir.$fileName);
                $formvar->setFoto($fileName);
             }else{
                $formvar->setFoto("unloaded");
             }

            $administradorObj = $entityManager->getReference('GranKoalaBundle:Administradores', $idAdministrador);
            $formvar->setIdAdministrador($administradorObj); 

            $estadoObj = $entityManager->getReference('GranKoalaBundle:EspaciosPublicitariosEstados', 2);            
            $formvar->setIdEstado($estadoObj);
            $formvar->setPublico(0); 

            $tipoObj = $entityManager->getReference('GranKoalaBundle:EspaciosPublicitariosTipos', 1);
            $formvar->setIdTipo($tipoObj);           
            $formvar->setIdEstado(NULL);           

            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('espaciospublicitarios_koala_inicio');        
        }
        return $this->render('Koala/defaults/new.html.twig', array(            
            'form' => $form->createView(),
            'titulo' => "Espacio Publicitario", 
        ));
    } 

    public function editAction(Request $request, EspaciosPublicitarios $formvar){
        $editForm = $this->createForm("App\Gran\KoalaBundle\Form\EspaciosPublicitariosEditType", $formvar);
        $editForm->handleRequest($request);
        $ArchivoDir = $this->container->getParameter('kernel.root_dir').'/../public/images/espaciospublicitarios/';

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $postdata = $request->request->all();

            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('espaciospublicitarios_koala_inicio');
        }

        return $this->render('Koala/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Espacio Publicitario",
            'nombre' => "espaciospublicitarios",
            'foto' => $formvar->getFoto(),
        ));
    }  

    public function deleteAction(Request $request, $id){        
        $entityManager = $this->getDoctrine()->getManager();
        $delete = $entityManager->getRepository('GranKoalaBundle:EspaciosPublicitarios')->find($id);
        $delete->setEliminado(1);
        $entityManager->persist($delete);
        $entityManager->flush($delete);
        return $this->redirectToRoute('espaciospublicitarios_koala_inicio');
    } 
}