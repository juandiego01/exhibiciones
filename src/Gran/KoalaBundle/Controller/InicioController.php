<?php
// src/Gran/KoalaBundle/Controller/InicioController.php
namespace App\Gran\KoalaBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class InicioController extends Controller
{    
    public function index(Request $request)
    {        

        $retVar = "";
        
        return $this->render('Koala/inicio/index.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'retvar' => $retVar,
                ));        
    }
    public function login(Request $request)
    {

        $retVar = "";
        
        return $this->render('Koala/inicio/login.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'retvar' => $retVar,
                ));   
    }
    public function loginError(Request $request)
    {        

        $retVar = "Error";
        
        return $this->render('Koala/inicio/login.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'retvar' => $retVar,
                ));   
    }
}