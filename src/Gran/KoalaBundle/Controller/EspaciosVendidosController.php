<?php
// src/Gran/KoalaBundle/Controller/EspaciosVendidosController.php
namespace App\Gran\KoalaBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\KoalaBundle\Entity\EspaciosVendidos;
use App\Gran\KoalaBundle\Form\EspaciosVendidosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Cpdg\UsuarioBundle\Controller\GlobalesController;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class EspaciosVendidosController extends Controller
{    
    public function indexAction(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciosVendidos = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosVendidos")->createQueryBuilder('ev')
        ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
        ->leftJoin('GranKoalaBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.id = ev.idEspacio')
        ->leftJoin('GranKoalaBundle:Proveedores', 'p', 'WITH', 'p.id = ev.idProveedor')
        ->leftJoin('GranKoalaBundle:Usuarios', 'u', 'WITH', 'u.id = ev.idUsuario');
        
        if(@$request->query->get('buscar') != ""){
           $espaciosVendidos->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('p.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('u.usuario LIKE :usuario1')->setParameter('usuario1', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('ev.proveedorNombre LIKE :proveedorNombre')->setParameter('proveedorNombre', "%".$request->query->get('buscar')."%");
        }

        if(@$request->query->get('regional') != "Todas" && @$request->query->get('tbvr') == "1"){
           $espaciosVendidos->andWhere('u.regional = :regional')->setParameter('regional', $request->query->get('regional'));  
        }

        if(@$request->query->get('ciudad') != "Todas" && @$request->query->get('tbvr') == "1"){

           if($request->query->get('ciudad') != "Todas"){
                $espaciosVendidos->andWhere('u.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$request->query->get('ciudad')."%");
            }           
        }

        if(@$request->query->get('idespacio') != "" && @$request->query->get('tbvr') == "2"){

            $espaciosVendidos->andWhere('ep.idTipoEspacio = :idTipoEspacio')->setParameter('idTipoEspacio', $request->query->get('idespacio'));            
        }


        $espaciosVendidosExecute = $espaciosVendidos->orderBy("ev.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciosVendidosExecute,
            $pagev,$cantv
        );


        $espaciospublicitariostipos = $this->getDoctrine()->getRepository("GranKoalaBundle:UsuariosEspaciosTipos")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->getQuery()->execute();

        $ciudadesRegionales = $this->getDoctrine()->getRepository("GranKoalaBundle:Usuarios")->createQueryBuilder('u')
        ->groupBy("u.ciudad")->getQuery()->execute();
        $arrCiudadesRegionales = array();
        $arrCiudadesAll = array();
        foreach ($ciudadesRegionales as $fila) {
            $arrCiudadesRegionales[$fila->getRegional()][] = $fila->getCiudad();
            $arrCiudadesAll[] = $fila->getCiudad();
        }


        return $this->render('Koala/espaciosvendidos/index.html.twig', array(
            'titulo' => "Espacios negociados",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,  
            'arrCiudadesAll' => $arrCiudadesAll,          
            'buscar' => @$request->query->get('buscar'),   
            'errort' => @$request->query->get('errort'),
            'regional' => @$request->query->get('regional'),         
            'idespacio' => @$request->query->get('idespacio'),         
            'espaciospublicitariostipos' => @$espaciospublicitariostipos,  
            'arrCiudadesRegionales' => $arrCiudadesRegionales,                
                ));        
    }

    public function buscarAction(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciosVendidos = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosVendidos")->createQueryBuilder('ev')
        ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
        ->leftJoin('GranKoalaBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.id = ev.idEspacio')
        ->leftJoin('GranKoalaBundle:Proveedores', 'p', 'WITH', 'p.id = ev.idProveedor')
        ->leftJoin('GranKoalaBundle:Usuarios', 'u', 'WITH', 'u.id = ev.idUsuario');
        
        if(@$request->query->get('buscar') != ""){
           $espaciosVendidos->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('p.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('u.usuario LIKE :usuario1')->setParameter('usuario1', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('ev.proveedorNombre LIKE :proveedorNombre')->setParameter('proveedorNombre', "%".$request->query->get('buscar')."%");
           
        }

        $espaciosVendidosExecute = $espaciosVendidos->orderBy("ev.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciosVendidosExecute,
            $pagev,$cantv
        );


        $espaciospublicitariostipos = $this->getDoctrine()->getRepository("GranKoalaBundle:UsuariosEspaciosTipos")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->getQuery()->execute();

        $template = 'Koala/espaciosvendidos/buscar.html.twig';
        $espaciospublicitariosdisponibles = array();

        if(@$postdata["casevar"] == "1"){
            $espaciospublicitariosdisponibles = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->where('u.usuario = :usuario')->setParameter('usuario', $postdata["buscar"])
            ->andWhere('ep.idEstado = :idEstado')->setParameter('idEstado', 2)
            ->leftJoin('GranKoalaBundle:Usuarios', 'u', 'WITH', 'u.id = ep.idUsuario')
            ->getQuery()->execute();

            $template = 'Koala/espaciosvendidos/resultados.html.twig';            
        }elseif(@$postdata["casevar"] == "2"){
            
            $espaciospublicitariosdisponiblesq = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
                ->andWhere('ep.idEstado = :idEstado')->setParameter('idEstado', 2);
            $espaciospublicitariosdisponiblesq->andWhere('ep.idUsuario != :idUsuario')->setParameter('idUsuario', "");
            
            $espaciospublicitariosdisponiblesq->andWhere('ep.idTipoEspacio = :idTipoEspacio')->setParameter('idTipoEspacio', $postdata["buscarb"]);
            

            if($postdata["regional"] != "Todas"){
                $espaciospublicitariosdisponiblesq->andWhere('u.regional = :regional')->setParameter('regional', $postdata["regional"]);                                
            }
            if($postdata["ciudad"] != "Todas"){
                $espaciospublicitariosdisponiblesq->andWhere('u.ciudad LIKE :ciudad')->setParameter('ciudad', "%".$postdata["ciudad"]."%");
            }
            

            $espaciospublicitariosdisponiblesq->leftJoin('GranKoalaBundle:Usuarios', 'u', 'WITH', 'u.id = ep.idUsuario');

            $espaciospublicitariosdisponibles = $espaciospublicitariosdisponiblesq->getQuery()->execute();

            $entityManager = $this->getDoctrine()->getManager();
            $usuariosEspaciosTipos = $entityManager->getRepository('GranKoalaBundle:UsuariosEspaciosTipos')->find(@$postdata['buscarb']);

            $template = 'Koala/espaciosvendidos/resultadosb.html.twig'; 
        }

        $ciudadesRegionales = $this->getDoctrine()->getRepository("GranKoalaBundle:Usuarios")->createQueryBuilder('u')
        ->groupBy("u.ciudad")->getQuery()->execute();
        $arrCiudadesRegionales = array();
        $arrCiudadesAll = array();
        foreach ($ciudadesRegionales as $fila) {
            $arrCiudadesRegionales[$fila->getRegional()][] = $fila->getCiudad();
            $arrCiudadesAll[] = $fila->getCiudad();
        }



        return $this->render($template, array(
            'titulo' => "Buscar espacios",
            'resultset' => $resultset,
            'espaciospublicitariostipos' => $espaciospublicitariostipos,
            'espaciospublicitariosdisponibles' => $espaciospublicitariosdisponibles,
            'arrCiudadesAll' => $arrCiudadesAll,          
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'buscar' => @$postdata['buscar'],            
            'buscarb' => @$postdata['buscarb'],            
            'regional' => @$postdata['regional'],            
            'arrCiudadesRegionales' => $arrCiudadesRegionales,
            'usuariosEspaciosTipos' => @$usuariosEspaciosTipos,
                ));        
    }

    public function newAction(Request $request){
        $ret = 0;
        date_default_timezone_set('America/Bogota');
        $entityManager = $this->getDoctrine()->getManager();
        $formvar = new EspaciosVendidos();

        $errort = "";
        
        $form = $this->createForm("App\Gran\KoalaBundle\Form\EspaciosVendidosType", $formvar);        
        $form->handleRequest($request);
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idAdministrador=$objUsuario->getId();

        $ArchivoDir = $this->container->getParameter('kernel.root_dir').'/../public/images/espaciospublicitarios/';

        if ($form->isSubmitted() && $form->isValid()){
                $postdata = $request->request->all();
                $formvar->setValor(intval($formvar->getValor()));
            
                
                if($postdata["casevar"] != 3){
                
                $formvar->setFechaRadicacion(new \DateTime(date("Y-m-d H:i:s")));
                $formvar->setEliminado(0);
                

                 $fileName = md5(uniqid()).'.jpg';             
                 $imagen = $formvar->getFotoEspacio();
                 if($imagen != ""){
                    copy($imagen,$ArchivoDir.$fileName);
                    $formvar->setFotoEspacio($fileName);
                 }else{
                    $formvar->setFotoEspacio("unloaded");
                 }

                 $fileName = md5(uniqid()).'.jpg';             
                 $imagen = $formvar->getFotoMaterial();
                 if($imagen != ""){
                    copy($imagen,$ArchivoDir.$fileName);
                    $formvar->setFotoMaterial($fileName);
                 }else{
                    $formvar->setFotoMaterial("unloaded");
                 }

                
                if($postdata["casevar"] == "1"){

                    $usuariost = $this->getDoctrine()->getRepository("GranKoalaBundle:Usuarios")->createQueryBuilder('u')
                    ->where('u.usuario = :usuario')->setParameter('usuario', $postdata["drogueria"])
                    ->getQuery()->execute();
                    foreach ($usuariost as $value){
                        $idUsuario = $value->getId();
                    }

                    $usuarioObj = $entityManager->getReference('GranKoalaBundle:Usuarios', $idUsuario);
                    $formvar->setIdUsuario($usuarioObj); 

                    $espacioObj = $entityManager->getReference('GranKoalaBundle:EspaciosPublicitarios', $postdata["idEspacio"]);
                    $formvar->setIdEspacio($espacioObj); 

                    $formvar->setValor(intval($formvar->getValor()));

                    $formvar->setFechaInicial(new \DateTime($postdata["fechaInicial"]));
                    $formvar->setFechaFinal(new \DateTime($postdata["fechaFinal"]));

                    $proveedorObj = $entityManager->getReference('GranKoalaBundle:Proveedores', 100);
                    $formvar->setIdProveedor($proveedorObj);


                }elseif($postdata["casevar"] == "2"){

                    $espacioObj = $entityManager->getReference('GranKoalaBundle:EspaciosPublicitarios', $postdata["idEspacio"]);
                    $formvar->setIdEspacio($espacioObj); 

                    $usuarioObj = $entityManager->getReference('GranKoalaBundle:Usuarios', $postdata["idUsuario"]);
                    $formvar->setIdUsuario($usuarioObj); 

                   $formvar->setValor(intval($formvar->getValor()));

                    $formvar->setFechaInicial(new \DateTime($postdata["fechaInicial"]));
                    $formvar->setFechaFinal(new \DateTime($postdata["fechaFinal"]));  

                    $proveedorObj = $entityManager->getReference('GranKoalaBundle:Proveedores', 100);
                    $formvar->setIdProveedor($proveedorObj);

                }

                $administradorObj = $entityManager->getReference('GranKoalaBundle:Usuarioskoala', $idAdministrador);
                $formvar->setIdUsuarioKoalaSolicitud($administradorObj);            

                $estadoObj = $entityManager->getReference('GranKoalaBundle:EspaciosVendidosEstados', 1);
                $formvar->setIdEstado($estadoObj);   


                $espaciospublicitariosvendidos = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosVendidos")->createQueryBuilder('ev')
                ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
                ->andWhere('ev.idEspacio = :idEspacio')->setParameter('idEspacio', $postdata["idEspacio"])
                ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(1,2,3));
                $vendidosCount = 0;
                $fi = date('Y-m-d', strtotime($postdata["fechaInicial"]));
                $ff = date('Y-m-d', strtotime($postdata["fechaFinal"]));
                foreach($espaciospublicitariosvendidos->getQuery()->execute() as $k3 => $v3) {
                    $vi = date('Y-m-d', strtotime($v3->getFechaInicial()->format("Y-m-d")));
                    $vf = date('Y-m-d', strtotime($v3->getFechaFinal()->format("Y-m-d")));                    
                    $ret = $this->evalDateAction($vi, $vf, $fi, $ff);
                    if($ret == "false"){
                        $vendidosCount ++;
                    }
                }
                if($vendidosCount == 0){
                    $entityManager->persist($formvar);
                    $entityManager->flush($formvar);
                }else{
                    $errort = "Las fechas seleccionadas no están disponibles.";
                }                
                
            }else{
                

                $vusuarios = explode(",", $postdata["idUsuario"]);
                $vIdEspacio = explode(",", $postdata["idEspacio"]);
                for($x = 0; $x <= intval($postdata["counter"] - 1); $x++){
                     
                     $reserva = new EspaciosVendidos();

                     $ret = $x;
                     $fileName = md5(uniqid()).'.jpg';             
                     $imagen = $formvar->getFotoEspacio();
                     if($imagen != ""){
                        copy($imagen,$ArchivoDir.$fileName);
                        $reserva->setFotoEspacio($fileName);
                     }else{
                        $reserva->setFotoEspacio("unloaded");
                     }

                     $fileName = md5(uniqid()).'.jpg';             
                     $imagen = $formvar->getFotoMaterial();
                     if($imagen != ""){
                        copy($imagen,$ArchivoDir.$fileName);
                        $reserva->setFotoMaterial($fileName);
                     }else{
                        $reserva->setFotoMaterial("unloaded");
                     }

                    $espacioObj = $entityManager->getReference('GranKoalaBundle:EspaciosPublicitarios', $vIdEspacio[$x]);
                    $reserva->setIdEspacio($espacioObj); 

                    $usuarioObj = $entityManager->getReference('GranKoalaBundle:Usuarios', $vusuarios[$x]);
                    $reserva->setIdUsuario($usuarioObj);

                    $administradorObj = $entityManager->getReference('GranKoalaBundle:Usuarioskoala', $idAdministrador);
                    $reserva->setIdUsuariokoalaSolicitud($administradorObj);            

                    $estadoObj = $entityManager->getReference('GranKoalaBundle:EspaciosVendidosEstados', 1);
                    $reserva->setIdEstado($estadoObj);

                    $reserva->setValor(intval($formvar->getValor()));


                    $reserva->setFechaInicial(new \DateTime($postdata["fechaInicial"]));
                    $reserva->setFechaFinal(new \DateTime($postdata["fechaFinal"]));


                    $reserva->setProveedorNombre($formvar->getProveedorNombre());  
                    $reserva->setProveedorCedula($formvar->getProveedorCedula());  
                    $reserva->setProveedorCelular($formvar->getProveedorCelular());  
                    $reserva->setProveedorCargo($formvar->getProveedorCargo());  

                    $proveedorObj = $entityManager->getReference('GranKoalaBundle:Proveedores', 100);
                    $reserva->setIdProveedor($proveedorObj);

                    $reserva->setFechaRadicacion(new \DateTime(date("Y-m-d H:i:s")));
                    $reserva->setEliminado(0);

                    
                    $espaciospublicitariosvendidos = $this->getDoctrine()->getRepository("GranKoalaBundle:EspaciosVendidos")->createQueryBuilder('ev')
                    ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
                    ->andWhere('ev.idEspacio = :idEspacio')->setParameter('idEspacio', $vIdEspacio[$x])
                    ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(1,2,3));
                    $vendidosCount = 0;
                    $fi = date('Y-m-d', strtotime($postdata["fechaInicial"]));
                    $ff = date('Y-m-d', strtotime($postdata["fechaFinal"]));
                    foreach($espaciospublicitariosvendidos->getQuery()->execute() as $k3 => $v3) {
                        $vi = date('Y-m-d', strtotime($v3->getFechaInicial()->format("Y-m-d")));
                        $vf = date('Y-m-d', strtotime($v3->getFechaFinal()->format("Y-m-d")));                    
                        $ret = $this->evalDateAction($vi, $vf, $fi, $ff);
                        
                        if($ret == "false"){
                            $vendidosCount ++;
                        }
                    }
                    if($vendidosCount == 0){
                        $entityManager->persist($reserva);
                        $entityManager->flush($reserva);
                    }else{
                        $errort = "Algunas fechas seleccionadas no están disponibles, las demás fueron reservadas.";
                    }                
                    
                }
            }

            return $this->redirectToRoute('espaciosvendidos_koala_index', array("ret" => $ret, "errort" => $errort));        
        }


        $usuariosEspaciosTipossend = "";
        if(@$request->query->get('id1') == "1"){
            $usuariosEspaciosTipos = $entityManager->getRepository('GranKoalaBundle:EspaciosPublicitarios')->find($request->query->get('id2'));
            if($usuariosEspaciosTipos->getIdTipoEspacio()->getId() == 13){
                $usuariosEspaciosTipossend = $usuariosEspaciosTipos->getIdTipoEspacio()->getNombre()." - ".$usuariosEspaciosTipos->getNombreEspacio();
            }else{
                $usuariosEspaciosTipossend = $usuariosEspaciosTipos->getIdTipoEspacio()->getNombre();
            }
        }
        return $this->render('Koala/defaults/new.html.twig', array(            
            'form' => $form->createView(),
            'titulo' => "Reservar espacio", 
            'espaciosvendidosvar' => "true", 
            'espaciosVendidos' => "true", 
            'id1' => @$request->query->get('id1'),
            'id2' => @$request->query->get('id2'),
            'id3' => @$request->query->get('id3'),
            'id4' => @$request->query->get('id4'),
            'id5' => @$request->query->get('id5'),
            'id6' => @$request->query->get('id6'),
            'usuariosEspaciosTipos' => $usuariosEspaciosTipossend,
            'errort' => $errort,
        ));
    } 

    public function cambiarEstadoAction(Request $request, EspaciosVendidos $formvar){
        $editForm = $this->createForm("App\Gran\KoalaBundle\Form\EspaciosVendidosEstadoType", $formvar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('espaciosvendidos_koala_index');
        }
        return $this->render('Koala/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Cambiar estado a negociación",
        ));
    } 

    public function evalDateAction($registradaInicio,$registradaFin,$verificarInicio,$verificarFin){   
        $returnVar = "true";
        if($verificarFin < $registradaInicio){
            return "true";
        }
        if($verificarInicio > $registradaFin){
            return "true";
        }
        if($verificarInicio <= $registradaInicio && ($verificarFin >= $registradaInicio && $verificarFin <= $registradaFin)){
            return "false";
        }
        if($verificarInicio >= $registradaInicio && $verificarFin <= $registradaFin){
            return "false";
        }
        if($verificarInicio <= $registradaFin && $verificarFin >= $registradaFin){
            return "false";
        }
        return $returnVar;
    }
}