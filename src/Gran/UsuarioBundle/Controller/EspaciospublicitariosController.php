<?php
// src/Gran/UsuarioBundle/Controller/EspaciospublicitariosController.php
namespace App\Gran\UsuarioBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class EspaciospublicitariosController extends Controller
{    
    public function index(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciospublicitarios = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosPublicitarios")->createQueryBuilder('ep')        
        ->leftJoin('GranUsuarioBundle:Usuarios', 'u', 'WITH', 'u.id = ep.idUsuario')
        ->leftJoin('GranUsuarioBundle:EspaciosPublicitariosTipos', 'ept', 'WITH', 'ept.id = ep.idTipo');
        
        if(@$request->query->get('buscar') != ""){
           $espaciospublicitarios->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");           
           $espaciospublicitarios->orWhere('ep.descripcion LIKE :descripcion')->setParameter('descripcion', "%".$request->query->get('buscar')."%");
        }

        $espaciospublicitarios->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
                              ->andWhere('ep.publico = :publico')->setParameter('publico', 1)
                              ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 1);

        $espaciospublicitariosExecute = $espaciospublicitarios->orderBy("ep.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciospublicitariosExecute,
            $pagev,$cantv
        );

        return $this->render('Usuario/espaciospublicitarios/index.html.twig', array(
            'titulo' => "Tipos de Espacios",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'buscar' => @$request->query->get('buscar'),            
                ));        
    }
    public function login(Request $request)
    {
                
        return $this->render('Usuario/espaciospublicitarios/login.html.twig', array(
            'titulo' => "EspaciospublicitariosAplicacion",
            'namevar' => "EspaciospublicitariosAplicacion",            
                ));   
    }
    public function loginError(Request $request)
    {        
        
        return $this->render('Usuario/espaciospublicitarios/login.html.twig', array(
            'titulo' => "EspaciospublicitariosAplicacion",
            'namevar' => "EspaciospublicitariosAplicacion",            
                ));   
    }
}