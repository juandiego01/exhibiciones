<?php
// src/Gran/UsuarioBundle/Controller/InicioController.php
namespace App\Gran\UsuarioBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class InicioController extends Controller
{    
    public function index(Request $request)
    {

        $retVar = "";
        
        return $this->render('Usuario/inicio/index.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'retvar' => $retVar,
                ));        
    }
    public function login(Request $request)
    {
        $sendlogin = "false";
        $retvar = "";

         return $this->render('Usuario/inicio/login.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'retvar' => @$retvar,
            'sendlogin' => $sendlogin,
            'username' => @$datos["_username"], 
            'password' => @$datos["_password"],
                )); 
    }
    
    public function loginError(Request $request)
    {        

        $retvar = "Error";
        
        return $this->render('Usuario/inicio/login.html.twig', array(
            'titulo' => "InicioAplicacion",
            'namevar' => "InicioAplicacion",
            'sendlogin' => @$sendlogin,
            'username' => @$datos["_username"], 
            'password' => @$datos["_password"],
            'retvar' => $retvar,
                ));   
    }
}