<?php
// src/Gran/UsuarioBundle/Controller/EspaciosVendidosController.php
namespace App\Gran\UsuarioBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\UsuarioBundle\Entity\EspaciosVendidos;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class EspaciosVendidosController extends Controller
{    
    public function index(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idUsuario=$objUsuario->getId();

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciosVendidos = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosVendidos")->createQueryBuilder('ev')
        ->leftJoin('GranUsuarioBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.id = ev.idEspacio')
        ->leftJoin('GranUsuarioBundle:EspaciosVendidosEstados', 'eve', 'WITH', 'eve.id = ev.idEstado')
        ->leftJoin('GranUsuarioBundle:Proveedores', 'p', 'WITH', 'p.id = ev.idProveedor')
        ->leftJoin('GranUsuarioBundle:Usuarios', 'u', 'WITH', 'u.id = ev.idUsuario');
        
        if(@$request->query->get('buscar') != ""){
           $espaciosVendidos->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('p.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");
           $espaciosVendidos->orWhere('u.usuario LIKE :usuario1')->setParameter('usuario1', "%".$request->query->get('buscar')."%");

           $espaciosVendidos->orWhere('eve.estado LIKE :estado')->setParameter('estado', "%".$request->query->get('buscar')."%");

           $espaciosVendidos->orWhere('ev.proveedorNombre LIKE :proveedorNombre')->setParameter('proveedorNombre', "%".$request->query->get('buscar')."%");
        }
        $espaciosVendidos->andWhere('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
                         ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $idUsuario);
        $espaciosVendidosExecute = $espaciosVendidos->orderBy("ev.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciosVendidosExecute,
            $pagev,$cantv
        );       


        return $this->render('Usuario/espaciosvendidos/index.html.twig', array(
            'titulo' => "Espacios negociados",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,           
            'buscar' => @$request->query->get('buscar'),            
            'errort' => @$request->query->get('errort'),         
                ));        
    }

    public function newAction(Request $request){
        $formvar = new EspaciosVendidos();
        $form = $this->createForm("App\Gran\UsuarioBundle\Form\EspaciosVendidosType", $formvar);        
        $form->handleRequest($request);
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idUsuario = $objUsuario->getId();
        $errort = "";
        $ArchivoDir = $this->container->getParameter('kernel.root_dir').'/../public/images/espaciospublicitarios/';
        if ($form->isSubmitted() && $form->isValid()) {                    
            $entityManager = $this->getDoctrine()->getManager();
            $postdata = $request->request->all();

            $formvar->setValor(intval($formvar->getValor()));

            $formvar->setFechaRadicacion(new \DateTime(date("Y-m-d H:i:s")));
            $formvar->setEliminado(0);

            $espacioObj = $entityManager->getReference('GranUsuarioBundle:EspaciosPublicitarios', $postdata["idEspacio"]);
            $formvar->setIdEspacio($espacioObj);

            $usuarioObj = $entityManager->getReference('GranUsuarioBundle:Usuarios', $idUsuario);
            $formvar->setIdUsuario($usuarioObj);

            $evObj = $entityManager->getReference('GranUsuarioBundle:EspaciosVendidosEstados', 1);
            $formvar->setIdEstado($evObj);
            
             $fileName = md5(uniqid()).'.jpg';             
             $imagen = $formvar->getFotoEspacio();
             if($imagen != ""){
                copy($imagen,$ArchivoDir.$fileName);
                $formvar->setFotoEspacio($fileName);
             }else{
                $formvar->setFotoEspacio("unloaded");
             }

             $fileName = md5(uniqid()).'.jpg';             
             $imagen = $formvar->getFotoMaterial();
             if($imagen != ""){
                copy($imagen,$ArchivoDir.$fileName);
                $formvar->setFotoMaterial($fileName);
             }else{
                $formvar->setFotoMaterial("unloaded");
             }


            $formvar->setFechaInicial(new \DateTime($postdata["fechaInicial"]));
            $formvar->setFechaFinal(new \DateTime($postdata["fechaFinal"])); 

            
                $espaciospublicitariosvendidos = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosVendidos")->createQueryBuilder('ev')
                ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
                ->andWhere('ev.idEspacio = :idEspacio')->setParameter('idEspacio', $postdata["idEspacio"])
                ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(1,2,3));
                $vendidosCount = 0;
                $fi = date('Y-m-d', strtotime($postdata["fechaInicial"]));
                $ff = date('Y-m-d', strtotime($postdata["fechaFinal"]));
                foreach($espaciospublicitariosvendidos->getQuery()->execute() as $k3 => $v3) {
                    $vi = date('Y-m-d', strtotime($v3->getFechaInicial()->format("Y-m-d")));
                    $vf = date('Y-m-d', strtotime($v3->getFechaFinal()->format("Y-m-d")));                    
                    $ret = $this->evalDateAction($vi, $vf, $fi, $ff);
                    if($ret == "false"){
                        $vendidosCount ++;
                    }
                }
                if($vendidosCount == 0){
                    $entityManager->persist($formvar);
                    $entityManager->flush($formvar);
                }else{
                    $errort = "Las fechas seleccionadas no están disponibles.";
                }                
                


            return $this->redirectToRoute('espaciosvendidos_inicio', array("errort" => $errort)); 

        }else{
        $now = date("Y-m-d");
        $consultarespaciosDisponibles = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosVendidos")->createQueryBuilder('ev')
            ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $idUsuario)
            ->andWhere('ev.fechaInicial <= :fechaInicial')->setParameter('fechaInicial', $now)
            ->andWhere('ev.fechaFinal >= :fechaFinal')->setParameter('fechaFinal', $now)
            ->leftJoin('GranUsuarioBundle:EspaciosPublicitarios', 'ep', 'WITH', 'ep.id = ev.idEspacio')
            ->getQuery()->execute();
        $arrayOut  = array();
        foreach ($consultarespaciosDisponibles as $keyed => $valueed) {
            $arrayOut[] = $valueed->getIdEspacio()->getId();
        }
            
        $espaciosDisponiblesarr = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosPublicitarios")->createQueryBuilder('e')->where('e.idUsuario = :idUsuario')->setParameter('idUsuario', $idUsuario)
            ->getQuery()->execute();

        $espaciosDisponibles = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosPublicitarios")->createQueryBuilder('e')
        ->where('e.idUsuario = :idUsuario')->setParameter('idUsuario', $idUsuario)
        ->andWhere('e.idEstado = :idEstado')->setParameter('idEstado', 2)
        
            ->getQuery()->execute();

        }

        return $this->render('Usuario/defaults/new.html.twig', array(            
            'form' => $form->createView(),                        
            'espaciosDisponibles' => $espaciosDisponibles,  
            'espaciosVendidos' => "true",            
            'nombre' => "espaciosvendidos", 
            'titulo' => "Negociar espacio",
        ));
    }

    public function cambiarEstadoAction(Request $request, EspaciosVendidos $formvar){
        $editForm = $this->createForm("App\Gran\UsuarioBundle\Form\EspaciosVendidosEstadoType", $formvar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('espaciosvendidos_inicio');
        }
        return $this->render('Usuario/defaults/edit.html.twig', array(
            'form' => $editForm->createView(),            
            'titulo' => "Cambiar estado a negociación",
        ));
    }   
    public function evalDateAction($registradaInicio,$registradaFin,$verificarInicio,$verificarFin){   
        $returnVar = "true";
        if($verificarFin < $registradaInicio){
            return "true";
        }
        if($verificarInicio > $registradaFin){
            return "true";
        }
        if($verificarInicio <= $registradaInicio && ($verificarFin >= $registradaInicio && $verificarFin <= $registradaFin)){
            return "false";
        }
        if($verificarInicio >= $registradaInicio && $verificarFin <= $registradaFin){
            return "false";
        }
        if($verificarInicio <= $registradaFin && $verificarFin >= $registradaFin){
            return "false";
        }
        return $returnVar;
    }
}