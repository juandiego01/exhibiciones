<?php
// src/Gran/UsuarioBundle/Controller/EspaciosDisponiblesController.php
namespace App\Gran\UsuarioBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class EspaciosDisponiblesController extends Controller
{    
    public function index(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');

        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idUsuario=$objUsuario->getId();

        
        $espaciospublicitariostipos = $this->getDoctrine()->getRepository("GranUsuarioBundle:UsuariosEspaciosTipos")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->getQuery()->execute();

        $espaciospublicitariosdisponibles = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->where('ep.idUsuario = :idUsuario')->setParameter('idUsuario', $idUsuario)
            ->getQuery()->execute();

        
        $idsArrayBase = array();
        $espaciospublicitariosdefinidos = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 1);
        $arrayDefinidos = array();
        $predefinidos = "";
        $predefinidosCount = 0;
        foreach ($espaciospublicitariosdefinidos->getQuery()->execute() as $k => $v) {
            $arrayDefinidos[$v->getId()]["id"] = $v->getId();
            $arrayDefinidos[$v->getId()]["nombre"] = $v->getNombreEspacio();
            $predefinidos .= $v->getNombreEspacio().", ";
            $predefinidosCount++;
            $idsArrayBase[] = $v->getId();
        }
        $predefinidos = substr($predefinidos, 0, -2);        
        
        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $usuariosList = $this->getDoctrine()->getRepository("GranUsuarioBundle:Usuarios")->createQueryBuilder('u')
        ->andWhere('u.eliminado = :eliminado')->setParameter('eliminado', 0)
        ->andWhere('u.id = :id')->setParameter('id', $idUsuario);

        $usuarios = $this->getDoctrine()->getRepository("GranUsuarioBundle:Usuarios")->createQueryBuilder('u')
        ->andWhere('u.eliminado = :eliminado')->setParameter('eliminado', 0)
        ->andWhere('u.id = :id')->setParameter('id', $idUsuario);
        
        if(@$request->query->get('buscar') != ""){
           $usuarios->andWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");           
        }

        $solicitadosArray = array(); 
        $vendidosArray = array(); 
        $disponiblesArray = array(); 

        $usuariosListExecute = $usuariosList->orderBy("u.id","desc")->getQuery()->execute();
        $resultsetList = $paginator->paginate(
            $usuariosListExecute,
            $pagev,$cantv
        );        
        foreach ($resultsetList as $k1 => $v1) {
            $solicitados = "";
            $solicitadosCount = 0;
            $idsArray = array();
            $espaciospublicitariossoliciados = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosPublicitarios")->createQueryBuilder('ep')
            ->where('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ep.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId());
            
            foreach ($espaciospublicitariossoliciados->getQuery()->execute() as $k2 => $v2){
                $solicitados .= $v2->getNombreEspacio().", ";
                $solicitadosCount++;
                $idsArray[$v2->getId()] = $v2->getId();
            }
            $solicitados = substr($solicitados, 0, -2);            
            $solicitadosArray[$v1->getId()]["solicitados"] = $solicitadosCount;

            $arrayIn = array_merge($idsArrayBase, $idsArray);
            $now = date("Y-m-d");
            $espaciospublicitariosvendidos = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosVendidos")->createQueryBuilder('ev')
            ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ev.idEspacio IN (:idEspacio)')->setParameter('idEspacio', $arrayIn)
            ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(1,2,3))
            ->andWhere('ev.fechaInicial <= :fechaInicial')->setParameter('fechaInicial', $now)
            ->andWhere('ev.fechaFinal >= :fechaFinal')->setParameter('fechaFinal', $now)
            ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId());
            $vendidosCount = 0;
            foreach($espaciospublicitariosvendidos->getQuery()->execute() as $k3 => $v3) {
                $vendidosCount ++;
            }
            $vendidosArray[$v1->getId()] = $vendidosCount;
            $disponiblesArray[$v1->getId()] = count($arrayIn) - $vendidosCount;
        }
        
        $usuariosExecute = $usuarios->orderBy("u.id","desc")->getQuery()->execute();

        $resultset = $paginator->paginate(
            $usuariosExecute,
            $pagev,$cantv
        );  

        $totalnegociados = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosVendidos")->createQueryBuilder('ev')
            ->select('COUNT(ev.id)')
            ->where('ev.eliminado = :eliminado')->setParameter('eliminado', 0)
            ->andWhere('ev.idEspacio IN (:idEspacio)')->setParameter('idEspacio', $arrayIn)
            ->andWhere('ev.idEstado IN (:idEstado)')->setParameter('idEstado', array(3))
            ->andWhere('ev.idUsuario = :idUsuario')->setParameter('idUsuario', $v1->getId())
            ->getQuery()->getSingleScalarResult();

        return $this->render('Usuario/espaciosdisponibles/index.html.twig', array(
            'titulo' => "Espacios Publicitarios Disponibles",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'predefinidos' => $predefinidosCount,            
            'solicitados' => $solicitadosArray,            
            'vendidos' => $vendidosArray,  
            'disponibles' => $disponiblesArray,  
            'espaciospublicitariostipos' => $espaciospublicitariostipos,  
            'espaciospublicitariosdisponibles' => $espaciospublicitariosdisponibles,  
            'totalnegociados' => $totalnegociados,  
            'buscar' => @$request->query->get('buscar'),            
                ));        
    } 
}