<?php
// src/Gran/UsuarioBundle/Controller/CrearEspacioController.php
namespace App\Gran\UsuarioBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Gran\UsuarioBundle\Entity\EspaciosPublicitarios;
use App\Gran\UsuarioBundle\Form\EspaciosPublicitariosType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CrearEspacioController extends Controller
{    
    public function index(Request $request)
    {        
        $postdata = $request->request->all();
        $paginator  = $this->get('knp_paginator');
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idUsuario=$objUsuario->getId();

        $pagev = @$request->query->get('page') != "" ? $request->query->get('page'):1;
        $cantv = @$request->query->get('cant') != "" ? $request->query->get('cant'):20;

        $espaciospublicitarios = $this->getDoctrine()->getRepository("GranUsuarioBundle:EspaciosPublicitarios")->createQueryBuilder('ep')        
        ->leftJoin('GranUsuarioBundle:Usuarios', 'u', 'WITH', 'u.id = ep.idUsuario')
        ->leftJoin('GranUsuarioBundle:EspaciosPublicitariosTipos', 'ept', 'WITH', 'ept.id = ep.idTipo')
        ->leftJoin('GranUsuarioBundle:EspaciosPublicitariosEstados', 'epe', 'WITH', 'epe.id = ep.idEstado')
        ;
        
        if(@$request->query->get('buscar') != ""){
           $espaciospublicitarios->andWhere('ep.nombreEspacio LIKE :nombreEspacio')->setParameter('nombreEspacio', "%".$request->query->get('buscar')."%");           
           $espaciospublicitarios->orWhere('ep.descripcion LIKE :descripcion')->setParameter('descripcion', "%".$request->query->get('buscar')."%");
           $espaciospublicitarios->orWhere('u.usuario LIKE :usuario')->setParameter('usuario', "%".$request->query->get('buscar')."%");  
           $espaciospublicitarios->orWhere('epe.estado LIKE :estado')->setParameter('estado', "%".$request->query->get('buscar')."%");          
        }

        $espaciospublicitarios->andWhere('ep.eliminado = :eliminado')->setParameter('eliminado', 0)
                              ->andWhere('ep.idTipo = :idTipo')->setParameter('idTipo', 2)
                              ->andWhere('ep.idUsuario = :idUsuario')->setParameter('idUsuario', $idUsuario);

        $espaciospublicitariosExecute = $espaciospublicitarios->orderBy("ep.id","desc")->getQuery()->execute();
        
        $resultset = $paginator->paginate(
            $espaciospublicitariosExecute,
            $pagev,$cantv
        );

        return $this->render('Usuario/crearespacio/index.html.twig', array(
            'titulo' => "Espacios Solicitados",
            'resultset' => $resultset,
            'pagev' => $pagev,
            'cantv' => $cantv,            
            'buscar' => @$request->query->get('buscar'),            
                ));        
    }

    public function new(Request $request){

        $formvar = new EspaciosPublicitarios();
        $form = $this->createForm("App\Gran\UsuarioBundle\Form\EspaciosPublicitariosType", $formvar);        
        $form->handleRequest($request);
        $objUsuario = $this->get('security.token_storage')->getToken()->getUser('Article', 1);
        $idUsuario=$objUsuario->getId();

        $ArchivoDir = $this->container->getParameter('kernel.root_dir').'/../public/images/espaciospublicitarios/';
        if ($form->isSubmitted() && $form->isValid()) {                    
            $entityManager = $this->getDoctrine()->getManager();
            $postdata = $request->request->all();

            $formvar->setFechaCreacion(new \DateTime(date("Y-m-d H:i:s")));
            $formvar->setEliminado(0);
            
             $fileName = md5(uniqid()).'.jpg';             
             $imagen = $formvar->getFoto();
             if($imagen != ""){
                copy($imagen,$ArchivoDir.$fileName);
                $formvar->setFoto($fileName);
             }else{
                $formvar->setFoto("unloaded");
             }
             $tmp = explode(",", $formvar->getNombreEspacio());
             if($formvar->getNombreEspacio() == "13"){
                $formvar->setNombreEspacio($postdata["otroInput"]);
                $tmp[1] = 13;
             }else{                
                $formvar->setNombreEspacio($tmp[0]);
             }

             $tipoEspacioObj = $entityManager->getReference('GranUsuarioBundle:UsuariosEspaciosTipos', $tmp[1]);
             $formvar->setIdTipoEspacio($tipoEspacioObj); 
             
            $usuarioObj = $entityManager->getReference('GranUsuarioBundle:Usuarios', $idUsuario);
            $formvar->setIdUsuario($usuarioObj); 

            $tipoObj = $entityManager->getReference('GranUsuarioBundle:EspaciosPublicitariosTipos', 2);
            $formvar->setIdTipo($tipoObj); 

            $tipoObjb = $entityManager->getReference('GranUsuarioBundle:EspaciosPublicitariosTiposb', 2);
            $formvar->setIdTipob($tipoObjb);    

            $estadoObj = $entityManager->getReference('GranUsuarioBundle:EspaciosPublicitariosEstados', 1);            
            $formvar->setIdEstado($estadoObj);
            $formvar->setPublico(1);           

            $entityManager->persist($formvar);
            $entityManager->flush($formvar);
            return $this->redirectToRoute('crearespacio_inicio');        
        }
        return $this->render('Usuario/defaults/new.html.twig', array(            
            'form' => $form->createView(),
            'titulo' => "Solicitud de Espacio Publicitario", 
            'crear_espacio' => "true",             
        ));
    } 
    
}