<?php

namespace App\Gran\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EspaciosVendidosEstadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('idEstado', EntityType::class, array(
                    'class' => 'GranUsuarioBundle:EspaciosVendidosEstados',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')  
                            ->where('e.id IN (:id)')->setParameter('id', array(1,2,3,4))                          
                            ->orderBy('e.id', 'ASC');
                    },
                    'choice_label' => function ($bodegas) {
                        return $bodegas->getEstado();
                    },
                    'label' => "Estado: ",                    
                ))  
        ;
    }    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Gran\UsuarioBundle\Entity\EspaciosVendidos'
        ));
    }
}
