<?php

namespace App\Gran\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EspaciosPublicitariosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder 

            ->add('nombreEspacio', ChoiceType::class, array(
                'choices' => array(
                    'ANTENAS DE SEGURIDAD' => 'ANTENAS DE SEGURIDAD,1',
                    'CANASTILLAS Y  CARROS' => 'CANASTILLAS Y  CARROS,2',
                    'MOSTRADOR' => 'MOSTRADOR,3',
                    'PUBLICIDAD  DEL RETABLO' => 'PUBLICIDAD  DEL RETABLO,4',
                    'GABINETES' => 'GABINETES,5',
                    'ESTANTERIA DETRAS DE MOSTRADOR HABLADOR EN LA PARTE SUPERIOR Y/O CAJA DE LUZ' => 'ESTANTERIA DETRAS DE MOSTRADOR HABLADOR EN LA PARTE SUPERIOR Y/O CAJA DE LUZ,6',
                    'GONDOLAS' => 'GONDOLAS,7',
                    'PANTALLAS' => 'PANTALLAS,8',
                    'CORRESPONSAL BANCARIO' => 'CORRESPONSAL BANCARIO,9',
                    'PUNTAS DE GONDOLAS' => 'PUNTAS DE GONDOLAS,10',
                    'VIDRIOS' => 'VIDRIOS,11',
                    'VALLAS INTERNAS' => 'VALLAS INTERNAS,12',
                    'OTROS' => '13',
                ),
                'label'    => 'Regional',
                'required'    => true,
                'empty_data'  => null,
                'attr' => array('onchange' => 'validateother(this)'), 
            ))

            ->add('otroEspacio', TextType::class, array(
                'mapped' => false,
                'required' => false,
                'label' => "Otro tipo de espacio: ",
                'attr' => array('disabled' => 'disabled', 'onkeyup' => 'setother(event, this.value)'), 

            ))

            ->add('descripcion', TextareaType::class, array(
                'required' => true,
                'label' => "Descripción: ",
            ))
            ->add('alto', IntegerType::class, array(
                'required' => true,
                'label' => "Alto en Centímetros: ",
            ))
            ->add('ancho', IntegerType::class, array(
                'required' => true,
                'label' => "Ancho en Centímetros: ",
            ))
            ->add('foto', FileType::class, array(
                'required' => false,
                'label'    => 'Adjuntar foto del espacio, La imágen debe estar en formato jpg',                
                'attr' => array('onchange' => 'validarimagen(this)'),                
                'data' => '',
            ))   
        ;
    }    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Gran\UsuarioBundle\Entity\EspaciosPublicitarios'
        ));
    }
}
