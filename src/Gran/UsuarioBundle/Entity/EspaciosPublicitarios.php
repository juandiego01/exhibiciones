<?php

namespace App\Gran\UsuarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EspaciosPublicitarios
 *
 * @ORM\Table(name="espacios_publicitarios", indexes={@ORM\Index(name="id_usuario", columns={"id_usuario"}), @ORM\Index(name="id_estado", columns={"id_estado"}), @ORM\Index(name="id_tipo", columns={"id_tipo"}), @ORM\Index(name="id_administrador", columns={"id_administrador"}), @ORM\Index(name="id_tipob", columns={"id_tipob"}), @ORM\Index(name="id_tipo_espacio", columns={"id_tipo_espacio"})})
 * @ORM\Entity
 */
class EspaciosPublicitarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_espacio", type="string", length=512, nullable=false)
     */
    private $nombreEspacio;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=false)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="alto", type="string", length=255, nullable=true)
     */
    private $alto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ancho", type="string", length=255, nullable=true)
     */
    private $ancho;

    /**
     * @var string|null
     *
     * @ORM\Column(name="foto", type="string", length=512, nullable=true)
     */
    private $foto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="denegacion", type="string", length=512, nullable=true)
     */
    private $denegacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion = 'CURRENT_TIMESTAMP';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="publico", type="boolean", nullable=true, options={"default"="1"})
     */
    private $publico = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=false)
     */
    private $eliminado = '0';

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @var \EspaciosPublicitariosEstados
     *
     * @ORM\ManyToOne(targetEntity="EspaciosPublicitariosEstados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado", referencedColumnName="id")
     * })
     */
    private $idEstado;

    /**
     * @var \EspaciosPublicitariosTipos
     *
     * @ORM\ManyToOne(targetEntity="EspaciosPublicitariosTipos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo", referencedColumnName="id")
     * })
     */
    private $idTipo;

    /**
     * @var \Administradores
     *
     * @ORM\ManyToOne(targetEntity="Administradores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_administrador", referencedColumnName="id")
     * })
     */
    private $idAdministrador;

    /**
     * @var \EspaciosPublicitariosTiposb
     *
     * @ORM\ManyToOne(targetEntity="EspaciosPublicitariosTiposb")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipob", referencedColumnName="id")
     * })
     */
    private $idTipob;

    /**
     * @var \UsuariosEspaciosTipos
     *
     * @ORM\ManyToOne(targetEntity="UsuariosEspaciosTipos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_espacio", referencedColumnName="id")
     * })
     */
    private $idTipoEspacio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreEspacio(): ?string
    {
        return $this->nombreEspacio;
    }

    public function setNombreEspacio(string $nombreEspacio): self
    {
        $this->nombreEspacio = $nombreEspacio;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getAlto(): ?string
    {
        return $this->alto;
    }

    public function setAlto(?string $alto): self
    {
        $this->alto = $alto;

        return $this;
    }

    public function getAncho(): ?string
    {
        return $this->ancho;
    }

    public function setAncho(?string $ancho): self
    {
        $this->ancho = $ancho;

        return $this;
    }

    public function getFoto(): ?string
    {
        return $this->foto;
    }

    public function setFoto(?string $foto): self
    {
        $this->foto = $foto;

        return $this;
    }

    public function getDenegacion(): ?string
    {
        return $this->denegacion;
    }

    public function setDenegacion(?string $denegacion): self
    {
        $this->denegacion = $denegacion;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getPublico(): ?bool
    {
        return $this->publico;
    }

    public function setPublico(?bool $publico): self
    {
        $this->publico = $publico;

        return $this;
    }

    public function getEliminado(): ?bool
    {
        return $this->eliminado;
    }

    public function setEliminado(bool $eliminado): self
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    public function getIdUsuario(): ?Usuarios
    {
        return $this->idUsuario;
    }

    public function setIdUsuario(?Usuarios $idUsuario): self
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    public function getIdEstado(): ?EspaciosPublicitariosEstados
    {
        return $this->idEstado;
    }

    public function setIdEstado(?EspaciosPublicitariosEstados $idEstado): self
    {
        $this->idEstado = $idEstado;

        return $this;
    }

    public function getIdTipo(): ?EspaciosPublicitariosTipos
    {
        return $this->idTipo;
    }

    public function setIdTipo(?EspaciosPublicitariosTipos $idTipo): self
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    public function getIdAdministrador(): ?Administradores
    {
        return $this->idAdministrador;
    }

    public function setIdAdministrador(?Administradores $idAdministrador): self
    {
        $this->idAdministrador = $idAdministrador;

        return $this;
    }

    public function getIdTipob(): ?EspaciosPublicitariosTiposb
    {
        return $this->idTipob;
    }

    public function setIdTipob(?EspaciosPublicitariosTiposb $idTipob): self
    {
        $this->idTipob = $idTipob;

        return $this;
    }

    public function getIdTipoEspacio(): ?UsuariosEspaciosTipos
    {
        return $this->idTipoEspacio;
    }

    public function setIdTipoEspacio(?UsuariosEspaciosTipos $idTipoEspacio): self
    {
        $this->idTipoEspacio = $idTipoEspacio;

        return $this;
    }


}
